"""
brncomp - module for symbolic and derivative computations in biochemical networks

Methods:
brncomp.jacobian - computation of Jacobians using sympy
brncomp.linearapproximation - compute matrices for a linear approximation
"""
# Copyright (C) 2010 Steffen Waldherr waldherr@ist.uni-stuttgart.de
# Time-stamp: <Last change 2012-10-02 14:36:31 by Steffen Waldherr>

from __future__ import division

import numpy as np
import re
import math

try:
    import sympy as sym
    have_sympy = True
except ImportError:
    have_sympy = False

def jacobian(f,x,vars=[],valuedict={},returntype="fun"):
    """
    compute the Jacobian of f with respect to x
    f and x are string lists
    vars is a list of lists of independent variables, which should be maintained
    as variables in the resulting Jacobian.
    if vars is empty, take vars=[x]
    valuedict is a dict of symbols to be substituted by either a number or
      a symbolic expression, with a substitution depth up to two, i.e. values in
      valuedict may themselves contain keys in valuedict, but these keys most contain
      only numbers or symbols from x or param.
    returntype may be one of:
      "fun" - return a lambda function for the jacobian, arguments are lists of independent
              variables as given in the tuple 'vars'
      "str" - return the jacobian as list of symbolic expressions
      "sym" - return as sympy expressions

    usage example:
    >>> jacobian(['f'],['x'],[['x'],['k']],valuedict={'f':'k*x**2'},returntype="str")
    [['2*k*x']]
    >>> fun = jacobian(['f'],['x'],[['x'],['k']],valuedict={'f':'k*b','b':'x**2'},returntype="fun")
    >>> fun([2],[2])
    [[8]]
    """
    if not have_sympy:
        raise ImportError("jacobian requires the sympy package to be installed.")

    if not len(vars):
        vars = [x]
    varlist = []
    for i in vars:
        varlist += i
    symvars = [sym.Symbol(i) for i in varlist]
    symdict = dict(zip(varlist,symvars))
    symx = [eval(i,sym.__dict__,symdict) for i in x]
    for i,j in zip(x,symx):
        symdict[i] = j

    symdict2nd = {}
    for i,v in valuedict.items():
        if not i in varlist:
            if v.__class__ is "".__class__:
                try:
                    symdict[i] = eval(v,sym.__dict__,symdict)
                except (NameError, TypeError):
                    symdict2nd[i] = v
            elif np.isscalar(v) and np.isreal(v):
                symdict[i] = float(v)
            else:
                raise ValueError("Data type of %s must be string or real number, but it is %s." %
                                 (i,str(v.__class__)))
    while len(symdict2nd):
        l = len(symdict2nd)
        for i,v in symdict2nd.items():
            try:
                symdict[i] = eval(v,sym.__dict__,symdict)
                del symdict2nd[i]
            except NameError:
                pass
        if l == len(symdict2nd):
            raise ValueError("Could not resolve some expressions in valuedict. Please check that no algebraic loops are present in variable definitions")
    symf = [eval(i,sym.__dict__,symdict) for i in f]

    jacsym = list()
    jacstr = list()
    for i in symf:
        jacsym.append(list())
        jacstr.append(list())
        for j in symx:
            dfdx = sym.powsimp(sym.diff(i,j))
            jacsym[-1].append(dfdx)
            jacstr[-1].append(sym.printing.ccode(dfdx))

    if returntype is "str":
        return jacstr
    elif returntype is "sym":
        return jacsym
    elif returntype is "fun":
        listsyms = [[sym.Symbol("listarg"+str(i)+"_"+str(j)) for j in xrange(len(v))]
                    for i,v in enumerate(vars)]
        listsymsflat = []
        for i in listsyms:
            listsymsflat += i
        # substitute the x and param in jacsym by list...
        jacsymnew = [[j.subs(zip(symvars,listsymsflat)) for j in i] for i in jacsym]
        jacstrnew = re.sub(r'_([0-9]+)',r'[\1]',str(jacsymnew))
        argstr = ",".join(["listarg"+str(i) for i in xrange(len(vars))])
        lambdastr = "lambda "+argstr+": "+jacstrnew
#         listxsym = [sym.Symbol("listxarg_" + str(i)) for i in xrange(len(x))]
#         listparamsym = [sym.Symbol("listparamarg_" + str(i)) for i in xrange(len(param))]
#         jacsymnew = [[j.subs(zip(symx+symp,listxsym+listparamsym)) for j in i] for i in jacsym]
#         jacstrnew = re.sub(r'_([0-9]+)',r'[\1]',str(jacsymnew))
#         lambdastr = "lambda listxsym,listparamsym=[]: " + jacstrnew
        return eval(lambdastr,math.__dict__,{})
    else:
        raise ValueError("Argument 'returntype' must be either 'fun' or 'str'")

def linearapproximation(net, x=None, p=None, inputs=None, outputs=None, returntype='fun'):
    """
    compute matrices for a linear approximation of Brn 'net'.

    Arguments:
    returntype - one of
        'fun': resulting matrix M is a function to evaluate as M(x,p)
        'num': resulting matrix M is a numpy array, evaluated at species 'x' and parameters 'p'
               using default values for 'x' or 'p' if they are None
    x, p - custom species, parameter values
    inputs - list of parameters to be considered as input variables
    outputs - list of symbolic expressions to be considered as output variables

    Compute the linear approximations A = df/dz, B = df/du, and C = dh/dx, where f is
    the network's ODE right hand side, z the state variable, u the input variables,
    and h the output functions.

    Returns (A,B,C), where B and / or C are None if inputs / outputs is.
    """
    xplist = [net.species, net.parameters]
    dvdxfun = jacobian(net.reactions, net.species, xplist, valuedict=net.values)
    haveconservation = net.has_conservation
    if 'conserv' not in net.__dict__ and haveconservation:
        net.conservation_analysis()
    scalefun = net.scale_fun()
    if haveconservation:
        Nr = net.conserv.Nr
        L = net.conserv.L
    else:
        Nr = np.asarray(net.stoich)
    def Afun(myx, myp, scale=None):
        """
        A = Nr sigma(p) dv/dx(x,p) Si(p) L
        """
        if scale is None:
            scale = scalefun(myp)
        sigma = np.max(scale)
        si = np.float64(scale) / sigma
        if haveconservation:
            As = np.dot(np.dot(Nr, dvdxfun(myx,myp)) * si, L)
        else:
            As = np.dot(Nr, dvdxfun(myx,myp)) * si
        return sigma * As
    if inputs is not None:
        dvdufun = jacobian(net.reactions, inputs, xplist, valuedict=net.values)
        def Bfun(myx, myp, scale=None):
            """
            B = Nr sigma(p) dv/du(x,p)
            """
            if scale is None:
                scale = scalefun(myp)
            sigma = np.max(scale)
            return sigma * np.dot(Nr, dvdufun(myx,myp))
    else:
        Bfun = None
    if outputs is not None:
        dhdxfun = jacobian(outputs, net.species, xplist, valuedict=net.values)
        def Cfun(myx, myp):
            """
            C = dh/dx(x,p) L
            """
            if haveconservation:
                return np.dot(dhdxfun(myx, myp), L)
            else:
                return dhdxfun(myx, myp)
    else:
        Cfun = None
    if returntype == "fun":
        return(Afun, Bfun, Cfun)
    elif returntype == "num":
        if x is None:
            x = net.eval(net.species)
        if p is None:
            p = net.eval(net.parameters, level="param")
        scale = scalefun(p)
        A = Afun(x,p,scale)
        B = Bfun(x,p,scale) if Bfun is not None else None
        C = Cfun(x,p) if Cfun is not None else None
        return (A,B,C)

def variable_substitution(vardict, params):
    """
    Substitute variable references in symbolic expressions.
    'vardict' is a dict with variable names as keys and formulas as values. Formulas
    may depend on other variables and symbolic parameters given in the list 'params'.
    Tries to substitute all references to other variables in the variable formulas by
    the corresponding expressions.

    May raise ValueError if problems arise, e.g. due to an algebraic loop.
    """
    if not have_sympy:
        raise ImportError("variable_substitution requires the sympy package to be installed.")
    symparams = [sym.Symbol(i) for i in params]
    symdict = dict(zip(params, symparams))
    symdict2nd = {}
    floatvars = {}
    for v in vardict:
        if vardict[v].__class__ is "".__class__:
            try:
                symdict[v] = eval(vardict[v], sym.__dict__, symdict)
                if isinstance(symdict[v], float) or isinstance(symdict[v], int):
                    floatvars[v] = float(symdict[v])
                    symdict[v] = sym.Symbol(v)
            except (NameError, TypeError):
                symdict2nd[v] = vardict[v]
        elif np.isscalar(vardict[v]) and np.isreal(vardict[v]):
            floatvars[v] = float(vardict[v])
            symdict[v] = sym.Symbol(v)
        else:
            raise ValueError("Data type of %s must be string or real number, but it is %s." %
                             (v,str(vardict[v].__class__)))
    while len(symdict2nd):
        l = len(symdict2nd)
        for i,v in symdict2nd.items():
            try:
                symdict[i] = eval(v,sym.__dict__,symdict)
                del symdict2nd[i]
            except (NameError, TypeError):
                pass
        if l == len(symdict2nd):
            # no advance in the last step, so we just call eval again without exception handling and hope for a useful
            # error message ...
            for i,v in symdict2nd.items():
                symdict[i] = eval(v,sym.__dict__,symdict)
                del symdict2nd[i]
            break

    for v in floatvars:
        symdict[v] = floatvars[v]
    resdict = dict((v, str(symdict[v])) for v in vardict)
    for v in resdict:
        try:
            resdict[v] = float(resdict[v])
        except:
            pass
    return resdict


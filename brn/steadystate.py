"""
modul for computation of steady states in biochemical networks
"""
# Copyright (C) 2010 Steffen Waldherr waldherr@ist.uni-stuttgart.de
# Time-stamp: <Last change 2011-07-15 10:16:57 by Steffen Waldherr>

import numpy as np
import brn
import brncomp
import brneval
import warnings

try:
    from scipy import optimize
    have_scipy=True
except ImportError, err:
    have_scipy=False
    no_scipy_message = "steadystate requires the scipy.optimize package, but import failed with error: %s" % str(err)


class SteadyState(object):
    """
    steady state computation for biochemical networks

    important methods:
    - SteadyState.set: set values for parameters and/or initial conditions
    - SteadyState.compute: solve for the steady state
    """
    def __init__(self,net,p=None,x0=None,values={},evaluator=brneval.PythonBrnEvaluator):
        """
        define a steady state computation problem for network 'net'

        input arguments:
        p - parameter values
        x0 - initial point for computation
        evaluator - class to use for evaluation of the ODE's right hand side
        """
        if not have_scipy:
            raise ImportError(no_scipy_message)
        if x0 is not None and len(x0) != len(net.species):
            raise ValueError("x0 = %s is not of correct dimension for steady state computation." % str(x0))
        self.evl = evaluator(net,p,x0,values)
        self.set = self.evl.set
#        eval.__init__(self,net,p,x0,values)
            
    def compute(self):
        """
        try to solve the steady state computation problem using scipy.optimize.fsolve
        returns xs, info, with:
        xs - result of computation
        info - dictionary of additional information, see scipy.optimize.fsolve documentation
        """
        evl = self.evl
        zs, infodict, ier, mesg = optimize.fsolve(evl.ode_rhs, evl.z0, fprime=evl.ode_jac if evl.havejac else None, full_output=1)
        zs = np.atleast_1d(zs)
        info = {'info':infodict, 'flag':ier, 'message':mesg}
        xs = evl.convert_points([zs])[0]
        return xs, info



def steadystate_branch(net,par,start,stop,step,p,x0):
    """
    compute a steady state branch for network 'net', with parameter 'par'
    varying from 'start' to 'stop'.
    the parameter is varied in steps of length 'step'
    additional arguments:
    p - parameter values to be used
    x0 - initial point for the computation

    output:
    plist - list of values for parameter 'par'
    xslist - list of steady states corresponding to the parameter values in 'plist'
    info - last output of the call to SteadyState.compute()
    """
    if step==0:
        step = float(stop-start)/10
    if (stop-start)*step < 0:
        step = -step

    myp = np.copy(p)
    myx0 = np.copy(x0)
    pindex = net.parameters.index(par)
    xslist = []
    plist = []
    ss = SteadyState(net,p=myp,x0=myx0)

    pval = start
    failed = False
    while (pval < stop if step > 0 else pval > stop) and not failed:
        myp[pindex] = pval
        plist.append(pval)
        ss.set(p=myp,x0=myx0)
        xs, info = ss.compute()
        xslist.append(np.copy(xs))
        pval += step
        
    return plist, xslist, info

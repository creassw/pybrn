"""
mathutils module: provides basic mathematics functionality for pybrn

functions:
rank - compute the matrix rank
"""

import numpy as np
import numpy.linalg as la

def rank(A):
    """
    Compute the rank of matrix A.
    """
    A = np.atleast_2d(A)
    u,s,vh = la.svd(A)
    maxabs = max(np.max(s), np.MachAr().eps)
    maxdim = max(A.shape)
    tol = maxabs * maxdim * np.MachAr().eps
    return np.sum(s>tol) # numerical rank of A

def null(A):
    """
    Compute the nullspace of matrix A.
    """
    A = np.atleast_2d(A)
    u,s,vh = la.svd(A, full_matrices=True)
    maxabs = max(np.max(s), np.MachAr().eps)
    maxdim = max(A.shape)
    tol = maxabs * maxdim * np.MachAr().eps
    if len(s) < A.shape[1]:
        s = np.concatenate((s, np.zeros(A.shape[1] - len(s))))
    if np.min(s) >= tol:
        return np.zeros((A.shape[1], 0))
    ind = np.nonzero(s<tol)[0]
    return vh.T[:, ind]
    return vh.T[np.ix_(range(A.shape[1]), ind)]

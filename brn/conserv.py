"""
conserv - provides methods for the analysis of conservation relations
"""
# Copyright (C) 2010 Steffen Waldherr waldherr@ist.uni-stuttgart.de
# Time-stamp: <Last change 2010-11-17 10:06:47 by Steffen Waldherr>

import numpy as np
import numpy.linalg as la

class Conservation(object):
    """
    provide tools for the analysis of networks with conserved moieties

    methods:
    full2reduced_fun - generate a lambda function for conversion from full to reduced coordinates
    reduced2full_fun - generate a lambda function for conversion from reduced to full coordinates
    """

    def __init__(self,net):
        """
        do automatic conservation analysis of the Brn 'net'

        sets the following attributes of this Conservation instance:
        rank - numerical rank of the stoichiometric matrix
        L - link matrix from SVD
        G - conservation matrix from SVD
        Nr - reduced stoichiometric matrix from SVD
        the attributes are related as follows:
        L * Nr is the orginal stoichiometric matrix
        G' * Nr = 0, i.e. each column of G describes a conservation relation
        L' * G = 0
        L' * L = I, G' * G = I
        """
        self.net = net
        u,s,vh = la.svd(np.atleast_2d(net.stoich))
        maxabs = np.max(s)
        maxdim = max(net.stoich.shape)
        tol = maxabs * maxdim * np.MachAr().eps
        self.rank = np.sum(s>tol)
        self.L = u[:,0:self.rank]
        self.G = u[:,self.rank:]
        self.Nr = np.vstack(s[i]*vh[i,:] for i in xrange(self.rank));

    def reduced2full_fun(self,x0):
        """
        return a lambda function for the conversion from the reduced coordinate system
        to the original coordinates

        the formula is x = L z + G G' x0
        """
        def the_reduced2full_fun(z,L=self.L,Gxbar=np.dot(self.G,np.dot(self.G.T,x0))):
            return np.dot(L,z) + Gxbar
        return the_reduced2full_fun

    def full2reduced_fun(self):
        """
        return a lambda function for the conversion from the original coordinate system
        to the reduced coordinates

        the formula is z = L' x
        """
        def the_full2reduced_fun(x,L=self.L):
            return np.dot(L.T,x)
        return the_full2reduced_fun


"""
numerical evaluation of Brns
"""
# Copyright (C) 2010 Steffen Waldherr waldherr@ist.uni-stuttgart.de
# Time-stamp: <Last change 2012-09-14 13:12:35 by Steffen Waldherr>

from __future__ import division

import numpy as np
import os
import sys
import math
import imp

import brn
import brncomp

import distutils.core
from distutils.extension import Extension
try:
    from Cython.Distutils import build_ext
    have_cython = True
except ImportError, err:
    have_cython=False
    cython_import_msg = str(err)

class BrnEvaluator(object):
    """
    Abstract class for numerical computation tasks:
    - interface for evaluating the network's ODE right hand side and Jacobian
    - useful for simulation and steady state computation

    Classes that do numerical computation can inherit from this class.
    """
    def __init__(self,net,p=None,x0=None,values={}):
        self.net = net
        self.havejac = brncomp.have_sympy
        if net.has_conservation:
            if not 'conserv' in net.__dict__:
                net.conservation_analysis()
        self.have_conservation = net.has_conservation
        if self.have_conservation:
            self.x2z = self.net.conserv.full2reduced_fun()
            self.N = np.asarray(self.net.conserv.Nr,dtype=np.float64)
        else:
            self.N = np.asarray(self.net.stoich,dtype=np.float64)
        self.have_scale = net.has_scale
        evl = ExpressionEvaluator(self.net, values=values) #, level="species")
        myx0 = evl.evalexpression(self.net.species) if x0 is None else x0
        myp = evl.evalexpression(self.net.parameters) if p is None else p
        self.set(p=myp, x0=myx0, values=values)
        # test for nan values in model evaluation
        evl.set(p=myp, x=myx0, values=values)
        myv = evl.evalexpression(self.net.reactions)
        if not np.all(np.isfinite(myv)):
            nanvals = [k for k in self.net.values if (np.isreal(self.net.values[k]) and not np.isfinite(self.net.values[k]))]
            raise ValueError("Model equations for %s cannot be evaluated numerically. Nonnumeric values: %s" % (str(self.net), ", ".join(nanvals)))
    
    def set(self, p=None, x0=None, values={}):
        """
        sets parameter values to 'p' and initial point to 'x0' for this brn evaluation problem
        numerical values for x and p can also be given in dict values, these override
        settings from 'p' and 'x0'
        """
        if p is not None:
            self.p = np.asarray(p, dtype=np.float64)
        if x0 is not None:
            self.x0 = np.asarray(x0, dtype=np.float64)
        pset = (p != None)
        xset = (x0 != None)
        for k in values:
            if k in self.net.parameters:
                pset = True
                self.p[self.net.parameters.index(k)] = float(values[k])
            elif k in self.net.species:
                xset = True
                self.x0[self.net.species.index(k)] = float(values[k])
        # readjust scale from self.p
        if pset:
            if self.have_scale:
                scale = np.asarray(self.net.scale_fun()(self.p),dtype=np.float64)
                self.common_scale = np.max(scale)
                self.scale = scale/self.common_scale
            else:
                self.common_scale = 1.
                self.scale = None
        # readjust internal z0 from scale and self.x0
        if xset:
            if self.have_scale:
                xn0 = np.asarray(self.x0,dtype=np.float64)/self.scale
            else:
                xn0 = self.x0
            if self.have_conservation:
                self.z2x = self.net.conserv.reduced2full_fun(xn0)
                self.z0 = self.x2z(xn0)
            else:
                self.z0 = xn0

    def convert_points(self,z):
        """
        convert the list of vectors 'z' in the internal, reduced coordinates, to the original coordinate system

        returns:
        x - a list of vectors corresponding to z in the original coordinates
        """
        if np.asarray(z[0]).shape == (): # looks like the argument is a single state vector
            myz = [z]
            singlepoint = True
        else:
            myz = z
            singlepoint = False
        if self.have_conservation:
            xn = map(self.z2x,myz)
        else:
            xn = myz
        if self.have_scale:
            x = [np.asarray(i,dtype=np.float64)*self.scale for i in xn]
        else:
            x = xn
        return x[0] if singlepoint else x

    def ode_rhs(self,z):
        """
        evaluate the BRN's ODE right hand side
        This method must be implemented by classes derived from the BrnEvaluator class.
        """
        raise NotImplementedError(
            "BrnEvaluator.ode_rhs is an abstract method, please use the subclasses for actual evaluation.")

    def ode_jac(self,z):
        """
        evaluate the Jacobian of the BRN's ODE right hand side
        This method must be implemented by classes derived from the BrnEvaluator class.
        """
        raise NotImplementedError(
            "BrnEvaluator.ode_jac is an abstract method, please use the subclasses for actual evaluation.")


class ExpressionEvaluator(object):
    """
    Class to evaluate arbitrary expressions in the context of a given BRN.
    """
    def __init__(self,net,p=None,x=None,values={},typestrict=True,level="all"):
        """
        use typestrict = True to ensure that np.float64 types are returned
        for use with e.g. sympy, use typestrict = False
        level may be one of:
        "param" - can only evaluate expressions which depend on parameters
        "species" - can only evaluate expressions which depend on species and parameters
        "all" - can evaluate all expressions
        """
        self.net = net
        self.typestrict = typestrict
        self.level = 0 if level == "param" else (1 if level == "species" else 2)
        self.set(p,x,values)

    def set(self,p=None,x=None,values={}):
        """
        update internal values of this evaluator to values given
        in vectors p, x0, and dict values.
        values takes precedence over p, x0.

        Returns the updated ExpressionEvaluator object.
        """
        self.values = {}
        if self.level >= 2:
            self.values.update(self.net.values)
        else:
            for i in self.net.parameters:
                self.values[i] = self.net.values[i]
            if self.level:
                for i in self.net.species:
                    self.values[i] = self.net.values[i]
        # check whether p argument is actually a dict, allowing to be called as evl.set(values)
        if type(p) is dict:
            values = p
            p = None
        if p is not None:
            self.values.update(zip(self.net.parameters,p))
        if self.level and x is not None:
            self.values.update(zip(self.net.species,x))
        self.values.update(values)
        evallist = [p for p in self.net.parameters]
        if self.level:
            evallist.extend(self.net.species)
            if self.level >= 2:
                evallist.extend(self.net.variables+self.net.reactions)
        for i in evallist:
            if np.isscalar(self.values[i]) and np.isreal(self.values[i]):
                self.values[i] = np.float64(self.values[i])
            elif self.values[i].__class__ is "".__class__:
                self.values[i] = eval(self.values[i], math.__dict__, self.values)
            else:
                if self.typestrict:
                    raise ValueError("Data type of %s must be string or real number, but it is %s." %
                                     (i,str(self.values[i].__class__)))
        return self

    def evalexpression(self,expr):
        """
        evaluate a symbolic expression, given as string 'expr', with this evaluator.
        may also be a sequence of symbolic expressions
        """
        if type(expr) is str:
            if len(expr):
                res = eval(expr, math.__dict__, self.values)
                if self.typestrict:
                    res = np.float64(res)
            else:
                res = np.asarray([], dtype=np.float64)
        elif np.iterable(expr):
            if len(expr):
                exprarray = np.asarray(expr, dtype=object)
                if self.typestrict:
                    res = np.vectorize(lambda x:(eval(x, math.__dict__, self.values) if type(x) is str else np.float64(x)),
                                       otypes=[np.float64])(exprarray)
                else:
                    res = np.vectorize(lambda x:(eval(x, math.__dict__, self.values) if type(x) is str else x))(exprarray)
            else:
                dtype = np.float64 if self.typestrict else None
                res = np.asarray(expr, dtype=dtype)
        else:
            try:
                res = np.float64(expr)
            except (TypeError,ValueError):
                res = np.nan # should I print a warning here? nan values sometimes confusing in simulation
        return res


class PythonBrnEvaluator(BrnEvaluator):
    """
    Subclass of BrnEvaluator implementing the rhs functions directly in Python.
    A simple but slow implementation without any further dependencies.
    """
    def __init__(self,net,p=None,x0=None,values={}):
        BrnEvaluator.__init__(self,net,p,x0,values)
        self.ratefun = net.rate_fun()
        if self.havejac:
            self.ratejacobian = net.jacobian(net.reactions,net.species)
    
    def ode_rhs(self,z):
        """
        Python implementation of the network's rhs
        fun = S N v(x,p)
        """
        x = self.convert_points(z)
        Nv = np.dot(self.N,self.common_scale*np.asarray(self.ratefun(x,self.p),dtype=np.float64))
        return Nv

    def ode_jac(self,z):
        """
        Python implementation of the network's rhs Jacobian
        jacfun = d ode_rhs / dx
        """
        x = self.convert_points(z)
        if self.have_scale:
            dvdxn = (np.asarray(self.ratejacobian(x,self.p),dtype=np.float64)
                    * np.asarray(self.scale,dtype=np.float64))
        else:
            dvdxn = np.asarray(self.ratejacobian(x,self.p),dtype=np.float64)
        if self.have_conservation:
            Ndvdz = np.dot(self.N, self.common_scale*np.asarray(np.dot(self.ratejacobian(x,self.p),
                                                                       self.net.conserv.L),dtype=np.float64))
        else:
            Ndvdz = np.dot(self.N,self.common_scale*np.asarray(self.ratejacobian(x,self.p),
                                                                        dtype=np.float64))
        return Ndvdz

class CythonBrnEvaluator(BrnEvaluator):
    """
    Subclass of BrnEvaluator that generates a Cython-compiled module to evaluate
    the network's right hand side.
    This should be faster than the PythonBrnEvaluator for many subsequent evaluations
    of the same network.

    Requires that Cython ( http://cython.org ) is installed.
    """
    def __init__(self,net,p=None,x0=None,values={},precompiled=None):
        """
        The argument 'precompiled' can be given as full path to a previously compiled module
        for the network 'net' (e.g. from the return value of the compile() method).
        The compilation is then skipped, and the given module is loaded directly.
        """
        BrnEvaluator.__init__(self,net,p,x0,values)
        if self.havejac:
            self.ratejacobian = [[net.jacobian([r],[x],returntype='str')[0][0] for x in net.species]
                                 for r in net.reactions]
        self.dim_x = len(net.species)
        self.dim_r = len(net.reactions)
        if self.have_conservation:
            self.dim_z = net.conserv.Nr.shape[0]
        else:
            self.dim_z = self.dim_x
        self.x = np.zeros(self.dim_x,dtype=np.float64)
        self.r = np.zeros(self.dim_r,dtype=np.float64)
        self.rhs = np.zeros(self.dim_z,dtype=np.float64)
        self.rjac = np.zeros((self.dim_r,self.dim_x),dtype=np.float64)
        self.jac = np.zeros((self.dim_z,self.dim_z),dtype=np.float64)
        myx0 = self.net.eval(self.net.species,values=values,level="species") if x0 is None else x0
        self.set(x0=myx0)
        self.compile(precompiled)
    
    def set(self,p=None,x0=None,values={}):
        """
        sets parameter values to 'p' and initial point to 'x0' for this brn evaluation problem
        """
        BrnEvaluator.set(self,p,x0,values=values)
        if x0 is not None or sum([i in values for i in self.net.species]):
            if self.have_scale:
                xn0 = np.asarray(self.x0,dtype=np.float64)/self.scale
            else:
                xn0 = np.asarray(self.x0,dtype=np.float64)
            if self.have_conservation:
                self.Gxbar = np.dot(self.net.conserv.G,np.dot(self.net.conserv.G.T,xn0))
                self.L = self.net.conserv.L
            else:
                self.Gxbar = None
                self.L = None
    
    def _template_str(self):
        return """
cimport numpy as np
cimport cython
from libc.math cimport *

ctypedef np.float64_t dtype_t

@cython.boundscheck(False)
@cython.wraparound(False)
def ode_rhs(np.ndarray[dtype_t,ndim=1] z not None, np.ndarray[dtype_t,ndim=1] p not None,
            np.ndarray[dtype_t,ndim=2] Nr not None, np.ndarray[dtype_t,ndim=1] x not None,
            np.ndarray[dtype_t,ndim=1] r not None, np.ndarray[dtype_t,ndim=1] rhs not None,
            np.ndarray[dtype_t,ndim=2] L=None, np.ndarray[dtype_t,ndim=1] Gxbar=None,
            np.ndarray[dtype_t,ndim=1] scale=None, dtype_t common_scale=1.):
    cdef Py_ssize_t i, j
    cdef int have_scale = %d
    cdef int have_conservation = %d
    cdef int dim_x = %d
    cdef int dim_z = %d
    cdef int dim_r = %d
    # test matrix shapes
    if (x.shape[0] != dim_x):
        raise ValueError("x must be of length " + str(dim_x))
    if (z.shape[0] != dim_z):
        raise ValueError("z must be of length " + str(dim_z))
    if (r.shape[0] != dim_r):
        raise ValueError("r must be of length " + str(dim_r))
    if (Nr.shape[0] != dim_z):
        raise ValueError("Nr must have " + str(dim_z) + " rows.")
    if (Nr.shape[1] != dim_r):
        raise ValueError("Nr must have " + str(dim_r) + "columns.")
    if (rhs.shape[0] != dim_z):
        raise ValueError("rhs must be of length " + str(dim_z))
    if have_conservation:
        if L is None:
            raise ValueError("L must not be None for a network with conservation.")
        if Gxbar is None:
            raise ValueError("Gxbar must not be None for a network with conservation.")
        if (L.shape[0] != dim_x or L.shape[1] != dim_z):
            raise ValueError("L must be a " + str(dim_x) + " x " + str(dim_z) + " matrix.")
        if (Gxbar.shape[0] != dim_x):
            raise ValueError("Gxbar must be of length " + str(dim_x))
    if have_scale:
        if scale is None:
            raise ValueError("scale must not be None for a network with scaling.")
        if (scale.shape[0] != dim_x):
            raise ValueError("scale must be of length " + str(dim_x))
    # compute x
    if have_conservation:
        # formula: x = L z + Gxbar
        for i in range(dim_x):
            x[i] = Gxbar[i]
            for j in xrange(dim_z):
                x[i] = x[i] + L[i,j]*z[j]
    else:
        for i in range(dim_x):
            x[i] = z[i]
    if have_scale:
        for i in range(dim_x):
            x[i] = x[i]*scale[i]
    # define species, parameters and variables
    %s
    # compute rates
    %s
    # compute rhs
    for i in range(rhs.shape[0]):
        rhs[i] = 0.
        for j in range(Nr.shape[1]):
            rhs[i] = rhs[i] + Nr[i,j]*r[j]
    return rhs

@cython.boundscheck(False)
@cython.wraparound(False)
def ode_jac(np.ndarray[dtype_t,ndim=1] z not None, np.ndarray[dtype_t,ndim=1] p not None,
            np.ndarray[dtype_t,ndim=2] Nr not None, np.ndarray[dtype_t,ndim=1] x not None,
            np.ndarray[dtype_t,ndim=2] rjac not None, np.ndarray[dtype_t,ndim=2] jac not None,
            np.ndarray[dtype_t,ndim=2] L=None, np.ndarray[dtype_t,ndim=1] Gxbar=None,
            np.ndarray[dtype_t,ndim=1] scale=None, dtype_t common_scale=1.):
    cdef Py_ssize_t i, j
    cdef int have_scale = %d
    cdef int have_conservation = %d
    cdef int dim_x = %d
    cdef int dim_z = %d
    cdef int dim_r = %d
    # test matrix shapes
    if (x.shape[0] != dim_x):
        raise ValueError("x must be of length " + str(dim_x))
    if (z.shape[0] != dim_z):
        raise ValueError("z must be of length " + str(dim_z))
    if (rjac.shape[0] != dim_r or rjac.shape[1] != dim_x):
        raise ValueError("rjac must be a " + str(dim_r) + " x " + str(dim_x) + "matrix.")
    if (Nr.shape[0] != dim_z):
        raise ValueError("Nr must have " + str(dim_z) + " rows.")
    if (Nr.shape[1] != dim_r):
        raise ValueError("Nr must have " + str(dim_r) + "columns.")
    if (jac.shape[0] != dim_z or jac.shape[1] != dim_z):
        raise ValueError("jac must be a quadratic matrix of dimension " + str(dim_z))
    if have_conservation:
        if L is None:
            raise ValueError("L must not be None for a network with conservation.")
        if Gxbar is None:
            raise ValueError("Gxbar must not be None for a network with conservation.")
        if (L.shape[0] != dim_x or L.shape[1] != dim_z):
            raise ValueError("L must be a " + str(dim_x) + " x " + str(dim_z) + " matrix.")
        if (Gxbar.shape[0] != dim_x):
            raise ValueError("Gxbar must be of length " + str(dim_x))
    if have_scale:
        if scale is None:
            raise ValueError("scale must not be None for a network with scaling.")
        if (scale.shape[0] != dim_x):
            raise ValueError("scale must be of length " + str(dim_x))
    # compute x
    if have_conservation:
        # formula: x = L z + Gxbar
        for i in range(dim_x):
            x[i] = Gxbar[i]
            for j in xrange(dim_z):
                x[i] = x[i] + L[i,j]*z[j]
    else:
        for i in range(dim_x):
            x[i] = z[i]
    if have_scale:
        for i in range(dim_x):
            x[i] = x[i]*scale[i]
    # define species, parameters and variables
    %s
    # compute ratejacobian
    %s
    # jac = Nr * rjac * L
    cdef dtype_t tempvar
    if have_conservation:
        for i in range(dim_z):
            for j in range(dim_z):
                jac[i,j] = 0.
                for k in range(dim_x):
                    tempvar = 0.
                    for l in range(dim_r):
                        tempvar += Nr[i,l]*rjac[l,k]
                    jac[i,j] += tempvar*L[k,j]
    else:
        for i in range(dim_z):
            for j in range(dim_z):
                jac[i,j] = 0.
                for k in range(dim_r):
                    jac[i,j] = jac[i,j] + Nr[i,k]*rjac[k,j]
    return jac
"""

    def _setup_template_str(self):
        return """# setup script for a pybrn compiled network
from distutils.core import setup
from distutils.extension import Extension
from Cython.Distutils import build_ext

ext_modules = [Extension("%s", ["%s.pyx"], libraries=["m"])]

setup(
  cmdclass = {'build_ext': build_ext},
  ext_modules = ext_modules
)
"""
    
    def compile(self,precompiled=None):
        """
        Compile the network into a loadable module with cython.
        
        If the same network has been compiled previously, give the full path
        (without extension) to the resulting module in the argument
        'precompiled', the compilation is then skipped.

        Returns the full path to the resulting module (without extension).
        """
        if not precompiled:
            if not have_cython:
                raise ImportError("Could not import Cython: " + cython_import_msg)
            # os.tempnam() on my Mac returns something like tmp.xxx, have to remove dots for valid modulname
            tempname = os.tempnam().replace(".", "_")
            dirname, modname = os.path.split(tempname)
    #         if dirname not in sys.path:
    #             sys.path.append(dirname)
            defsstr = ""
            for i,s in enumerate(self.net.species):
                defsstr += "cdef dtype_t %s = x[%d]" % (s,i)
                defsstr += '\n    '
            for i,p in enumerate(self.net.parameters):
                defsstr += "cdef dtype_t %s = p[%d]" % (p,i)
                defsstr += '\n    '
            for v in self.net.variables:
                defsstr += "cdef dtype_t %s = %s" % (v,self.net.values[v])
                defsstr += '\n    '
            rateevalstr = ""
            for i,r in enumerate(self.net.reactions):
                if self.have_scale:
                    rateevalstr += "r[%d] = (%s)*common_scale" % (i, self.net.values[r])
                else:
                    rateevalstr += "r[%d] = %s" % (i, self.net.values[r])
                rateevalstr += '\n    '
            rjacevalstr = ""
            if self.havejac:
                for i,r in enumerate(self.net.reactions):
                    for j in xrange(self.dim_x):
                        if self.have_scale:
                            rjacevalstr += "rjac[%d,%d] = %s*common_scale*scale[%d]" % (i,j,self.ratejacobian[i][j],j)
                        else:
                            rjacevalstr += "rjac[%d,%d] = %s" % (i,j,self.ratejacobian[i][j])
                        rjacevalstr += '\n    '
            filestr = self._template_str() % (self.have_scale, self.have_conservation,
                                              self.dim_x, self.dim_z, self.dim_r,
                                              defsstr, rateevalstr,
                                              self.have_scale, self.have_conservation,
                                              self.dim_x, self.dim_z, self.dim_r,
                                              defsstr, rjacevalstr)
            print tempname
            f = open(tempname + ".pyx",'w')
            f.write(filestr)
            f.close()
            # os.tempnam() on my Mac returns something like tmp.xxx, have to remove dots for valid scriptname
            setupname = os.tempnam().replace(".", "_")
            print setupname
            setupstr = self._setup_template_str() % (modname, tempname)
            f = open(setupname + ".py",'w')
            f.write(setupstr)
            f.close()
            cdir = os.getcwd()
            os.chdir(dirname)
            distutils.core.run_setup(setupname + ".py",script_args=['build_ext','--inplace'])
            os.chdir(cdir)
        else:
            dirname, modname = os.path.split(precompiled)
        f,p,d = imp.find_module(modname,[dirname])
        self.cnet = imp.load_module(modname,f,p,d)
        return os.path.join(dirname,modname)
        
    def ode_rhs(self,z):
        """
        call Cython implementation of the network's rhs
        fun = S N v(x,p)
        """
        return self.cnet.ode_rhs(np.asarray(z,dtype=np.float64),self.p,self.N,self.x,self.r,self.rhs,
                                 self.L,self.Gxbar,self.scale,np.float64(self.common_scale))
        
    def ode_jac(self,z):
        """
        call Cython implementation of the network's rhs Jacobian
        """
        return self.cnet.ode_jac(np.asarray(z,dtype=np.float64),self.p,self.N,self.x,self.rjac,self.jac,
                                 self.L,self.Gxbar,self.scale,np.float64(self.common_scale))
        

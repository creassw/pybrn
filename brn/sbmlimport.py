"""
methods to import sbml to pybrn data structure
"""
# Copyright (C) 2010 Steffen Waldherr waldherr@ist.uni-stuttgart.de
# Time-stamp: <Last change 2012-11-19 12:11:13 by Steffen Waldherr>

import numpy as np
import re
import brn
import warnings
import brncomp

try:
    import libsbml as sbml
    have_sbml=True
except ImportError, err:
    have_sbml=False
    sbml_import_msg = str(err)

class FunctionHelper(object):
    """
    Helper class to deal with SBML function definitions
    """
    def __init__(self, sbml_model):
        """
        initialize the FunctionHelper from the SBML model 'sbml_model', reading in all the function definitions
        in this model.
        """
        self.fundefs = []
        for fun in sbml_model.getListOfFunctionDefinitions():
            numargs = fun.getNumArguments()
            funstr = sbml.formulaToString(fun.getMath())
            fundef = funstr[funstr.find('(')+1:-1].split(', ', numargs)
            # fundef[-1] = self.process(fundef[-1])
            pattern = fun.getId() + r'\(' + r',\s*'.join([r'([^,)]*)' for i in range(numargs)]) + r'\)'
            substr = fundef[-1]
            for i in range(numargs):
                substr = re.sub(r'((?<=\W)|\A)' + fundef[i] + r'((?=\W)|\Z)', r'(\\%d)' % (i+1), substr)
            self.fundefs.append((pattern, substr))

    def process(self, expr):
        """
        Process expr by replacing all function calls with the corresponding formula
        """
        for fun in self.fundefs:
            # print fun[0], " -> ", fun[1], " in ", expr
            expr = re.sub(fun[0], fun[1], expr)
        return expr

def fromSBMLString(sbmlstring, **kwargs):
    """
    Generate a BRN from an SBML string
    Usage: net = fromSBML(sbmlstring)
    where sbmlstring is a string in SBML format and net is the resulting BRN.
    
    See readSBML() for accepted keyword options.
    """
    if not have_sbml:
        raise ImportError("SBML support requires the libsbml module, but importing this module failed with message: " + sbml_import_msg)
    reader = sbml.SBMLReader();
    doc = reader.readSBMLFromString(sbmlstring);
    return readSBML(doc, **kwargs)

def fromSBML(filename, **kwargs):
    """
    Import an SBML file as BRN.
    Usage: net = fromSBML(filename)
    where filename is the SBML file and net is the resulting BRN.

    See readSBML() for accepted keyword options.
    """
    if not have_sbml:
        raise ImportError("SBML support requires the libsbml module, but importing this module failed with message: " + sbml_import_msg)
    reader = sbml.SBMLReader();
    doc = reader.readSBML(filename);
    return readSBML(doc, **kwargs)

def readSBML(document, duplicatelocalparameters=False, undefined=np.nan, reduce_vars=False):
    """
    Convert an SBML document to a BRN.
    Usage: net = fromSBML(document)
    where document is a libsbml.SBMLDocument object and net is the resulting BRN.
    
    Set duplicatelocalparameters=True if local parameters with equal names should NOT be
    treated as the same parameters.

    'undefined' is used for any quantities without an explicit assignment in the SBML file,
    apart from compartments, which use 1.0 in this case.

    If 'reduce_vars' is true, variables that are not referenced by reaction rates are removed
    from the model.
    """
    if not have_sbml:
        raise ImportError("SBML support requires the libsbml module, but importing this module failed with message: " + sbml_import_msg)
    model = document.getModel();
    fh = FunctionHelper(model)
    m = model.getNumReactions();
    species = [];
    ic = {};
    parameters = [];
    pvals = {};
    variables = []
    varvals = {}
    reactions = [];
    rates = {};
    substanceunits = dict((s.getId(), s.getHasOnlySubstanceUnits()) for s in (model.getSpecies(i) for i in xrange(model.getNumSpecies())))
    compartment = {}
    scale = {}
    # get list of species and initial conditions
    # if the species unit is a concentration, we need to scale the ODE rhs by the compartment volume,
    # since reaction rates are in substance / time in SBML
    for s in (model.getSpecies(i) for i in xrange(model.getNumSpecies())):
        id = s.getId()
        compartment[id] = s.getCompartment()
        if s.getConstant() or s.getBoundaryCondition():
            if s.isSetInitialConcentration():
                if substanceunits[id]:
                    parameters += [id+"_concentration"]
                    pvals[id+"_concentration"] = s.getInitialConcentration()
                    variables += [id]
                    varvals[id] = id+"_concentration * "+compartment[id]
                else:
                    parameters += [id]
                    pvals[id] = s.getInitialConcentration()
            elif s.isSetInitialAmount():
                if substanceunits[id]:
                    parameters += [id]
                    pvals[id] = s.getInitialAmount()
                else:
                    parameters += [id+"_amount"]
                    pvals[id+"_amount"] = s.getInitialAmount()
                    variables += [id]
                    varvals[id] = id+"_amount / "+compartment[id]
            else:
                parameters += [id];
                pvals[id] = undefined
        else:
            species += [id];
            if s.isSetInitialConcentration():
                if substanceunits[id]:
                    parameters += [id+"_initial_concentration"]
                    pvals[id+"_initial_concentration"] = s.getInitialConcentration()
                    ic[id] = id+"_initial_concentration * "+compartment[id]
                else:
                    ic[id] = s.getInitialConcentration()
                    scale[id] = compartment[id]
            elif s.isSetInitialAmount():
                if substanceunits[id]:
                    ic[id] = s.getInitialAmount()
                else:
                    parameters += [id+"_initial_amount"]
                    pvals[id+"_initial_amount"] = s.getInitialAmount()
                    ic[id] = id+"_initial_amount / "+compartment[id]
                    scale[id] = compartment[id]
            else:
                ic[id] = undefined
                if not substanceunits[id]:
                    scale[id] = compartment[id]
    n = len(species);
    stoich = np.zeros((n,m));
    # get everything from reactions
    for (j,r) in enumerate(model.getReaction(i) for i in xrange(m)):
        reactions += [r.getId()];
        # get rate law
        k = r.getKineticLaw();
        if k is None:
            rates[r.getId()] = np.nan
        else:
            rates[r.getId()] = fh.process(sbml.formulaToString(k.getMath()))
            # get local parameters
            # local parameters must be constant by specification, so no need to check this...
            for lp in (k.getParameter(i) for i in xrange(k.getNumParameters())):
                if duplicatelocalparameters:
                    parameters += [lp.getId() + "_" + reactions[-1]];
                    pvals[lp.getId() + "_" + reactions[-1]] = lp.getValue() if lp.isSetValue() else undefined;
                    #rates[-1] = rates[-1].replace(lp.getId(), parameters[-1]);
                    rates[r.getId()] = re.sub("((?<=\W)|\A)"+lp.getId()+"((?=\W)|\Z)",parameters[-1],rates[r.getId()]);
                else:
                    if lp.getId() in parameters:
                        pvals[lp.getId()] = lp.getValue() if lp.isSetValue() else pvals[lp.getId()];
                    else:
                        parameters += [lp.getId()];
                        pvals[lp.getId()] = lp.getValue() if lp.isSetValue() else undefined;
        # get stoichiometry
        for thisspecies in r.getListOfReactants():
            if thisspecies.getSpecies() in species:
                xi = species.index(thisspecies.getSpecies());
                stoich[xi,j] -= thisspecies.getStoichiometry();
        for thisspecies in r.getListOfProducts():
            if thisspecies.getSpecies() in species:
                xi = species.index(thisspecies.getSpecies());
                stoich[xi,j] += thisspecies.getStoichiometry();
    # get global parameters and compartments
    for p in (model.getParameter(i) for i in xrange(model.getNumParameters())):
        if p.getId() in parameters:
            pvals[p.getId()] = p.getValue() if p.isSetValue() else pvals[p.getId()];
        else:
            if not p.getConstant(): # this is a variable
                variables += [p.getId()];
                varvals[p.getId()] = p.getValue() if p.isSetValue() else undefined
            else: # this is a parameter
                parameters += [p.getId()];
                pvals[p.getId()] = p.getValue() if p.isSetValue() else undefined;
    for p in (model.getCompartment(i) for i in xrange(model.getNumCompartments())):
        parameters += [p.getId()];
        pvals[p.getId()] = p.getSize() if p.isSetSize() else (p.getVolume() if p.isSetVolume() else 1.0) # if compartment size is undefined, use 1.0
    # print "Detected species: %s" % str(species)
    # print "Detected parameters: %s" % str(parameters)
    # check initial assignment rules
    symboldict = ic.copy();
    symboldict.update(pvals);
    for a in (model.getInitialAssignment(i) for i in xrange(model.getNumInitialAssignments())):
        s = a.getSymbol();
        if s in parameters:
            pvals[s] = fh.process(sbml.formulaToString(a.getMath()))
        elif s in variables:
            varvals[s] = fh.process(sbml.formulaToString(a.getMath()))
        elif s in species:
            ic[s] = fh.process(sbml.formulaToString(a.getMath()))
    for a in (model.getRule(i) for i in xrange(model.getNumRules())):
        if a.getElementName()=='assignmentRule':
            v = a.getVariable();
            f = fh.process(sbml.formulaToString(a.getMath()))
            # print "Assignment rule for %s detected, considering as variable: %s = %s" % (v, v, f)
            varvals[v] = f
            if v in species:
                i = species.index(v);
                stoich = np.delete(stoich,i,0);
                variables += [v]
                species.remove(v);
                ic.pop(v);
            elif v in parameters:
                variables += [v]
                parameters.remove(v);
                pvals.pop(v);
            elif v in variables:
                pass
            else:
                raise ValueError("Found an assignment rule for %s, but is not a parameter, variable, or species." % v)
        else:
                warnings.warn("Cannot handle SBML rule %s, ignoring." % a.getElementName())

    if reduce_vars:
        numvars = -1
        while numvars != len(variables):
            numvars = len(variables)
            for v in list(varvals.keys()):
                if ( np.all([re.search(r"((?<=\W)|\A)" + v + r"((?=\W)|\Z)", varvals[i]) is None for i in varvals if isinstance(varvals[i], str)]) and
                     np.all([re.search(r"((?<=\W)|\A)" + v + r"((?=\W)|\Z)", rates[i]) is None for i in rates if isinstance(rates[i], str)]) ):
                    variables.remove(v)
                    varvals.pop(v)
                    warnings.warn("Not importing SBML model variable %s, as it is not referenced by other variables or reactions." % v)
    varvals = brncomp.variable_substitution(varvals, parameters+species)
    # print variables
    scalevec = ["1./"+scale[s] if s in scale else 1 for s in species]
    net = brn.Brn(stoich,species,reactions,parameters=parameters,parvals=pvals,variables=variables,
                  varvals=varvals,rates=rates,ics=ic,scale=scalevec);
    return net;

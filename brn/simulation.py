"""
modul for numerical simulation of biochemical networks
"""
# Copyright (C) 2010 Steffen Waldherr waldherr@ist.uni-stuttgart.de
# Time-stamp: <Last change 2014-04-10 15:08:10 by Steffen Waldherr>

import numpy as np
import warnings
import imp
import os

import brn
import brneval
import brncomp

try:
    import scipy as sp
    import scipy.integrate as spi
    have_scipy=True
except ImportError:
    have_scipy=False

import distutils.core
from distutils.extension import Extension
try:
    from Cython.Distutils import build_ext
    have_cython = True
except ImportError, err:
    have_cython=False
    cython_import_msg = str(err)

class Simulator(object):
    """
    abstract class for simulation of biochemical networks

    Simulation classes should inherit from this class and at least redefine the run() method.
    """
    def __init__(self,net,times=[0, 1],x0=None,p=None,values={},evaluator=brneval.PythonBrnEvaluator,
                 outputs=("species",)):
        """
        net - biochemical network as Brn object to simulate
        times - list of time points for which simulation results are desired
        x0 - initial condition
        p - parameter values
        values - dictionary to take initial condition and parameters from
        evaluator - the BrnEvaluator subclass to use
        outputs - BRN variables to compute during simulation
        """
        self.net = net
        self.times = np.asarray(times,dtype=np.float64)
        self.evl = evaluator(net,p,x0,values)
        self.outputs = (outputs,) if outputs.__class__ is "".__class__ else outputs
    
    def initialize(self, x0=None, p=None, values={}):
        """
        set the initial condition for this simulation task to 'x0', a list or array of initial condition values.
        optionally also set the parameter values to 'p'
        """
        self.evl.set(p=p, x0=x0, values=values)
    
    def run(self):
        """
        Method to solve the simulation task.
        This method must be implemented by classes derived from the Simulator class.
        """
        raise NotImplementedError(
            "Simulator.run is an abstract method, please use the subclasses (e.g. VodeSimulator) for actual simulation")

    def convert_trajectory(self,z):
        """
        convert the list of vectors 'z' in the internal, reduced coordinates, to the original coordinate system

        returns:
        x - a list of vectors corresponding to z in the original coordinates
        """
        return self.evl.convert_points(z)

    def compute_outputs(self,z,outputs=("species",)):
        """
        from the list of vectors 'z' in internal coordinates, compute variables according
        to the sequence 'outputs'. Elements of 'outputs' may be 'species', 'reactions', 'variables',
        or named network elements.
        The outputs are returned as tuple in the order as they are given in 'outputs'.
        """
        outputvars = tuple([] for i in enumerate(outputs))
        xlist = self.evl.convert_points(z)
        if len(outputs)>1 or outputs[0] != "species":
            evl = brneval.ExpressionEvaluator(self.net, p=self.evl.p)
        for x in xlist:
            if len(outputs)>1 or outputs[0] != "species":
                evl.set(x=x)
            for o,ovar in zip(outputs,outputvars):
                if o == "species":
                    ovar.append(x)
                elif o == "reactions":
                    r = evl.evalexpression(self.net.reactions)
                    ovar.append(r)
                elif o == "variables":
                    v = evl.evalexpression(self.net.variables)
                    ovar.append(v)
                else:
                    n = evl.evalexpression(o)
                    ovar.append(n)
        return outputvars

class VodeSimulator(Simulator):
    """
    implements network simulation via scipy.integrate
    """
    def __init__(self,net,times=[0, 1],x0=None,p=None,values={},evaluator=brneval.PythonBrnEvaluator,outputs=("species",)):
        """
        arguments:        
        net - biochemical network as Brn object to simulate
        times - list of time points for which simulation results are desired
        x0 - initial conditions for network species
        p - parameter values
        evaluator - subclass of brneval.BrnEvaluator used to evaluate the ODE's rhs
        outputs - BRN variables to compute during simulation
        """
        if not have_scipy:
            raise ImportError("Could not import scipy, scipy.integrate. These are needed by VodeSimulator")
        Simulator.__init__(self,net,times,x0,p,evaluator=evaluator,outputs=outputs)
        self.with_jacobian = self.evl.havejac
        self.ode = spi.ode(lambda t,z: self.evl.ode_rhs(z),lambda t,z: self.evl.ode_jac(z))
        self.ode.set_integrator('vode',method='bdf',with_jacobian=self.with_jacobian)

    def run(self, times=None, outputs=None):
        """
        Method to solve the simulation task.
        Simulation data is generated at the time points given in the list 'times',
        and at default points if times=None.

        outputs - BRN variables to compute during simulation

        output:
        t - list of time points from simulation
        further outputs as specified in 'outputs', default is
        list of state values representing the trajectory
        """
        if times is None:
            times = self.times
        self.ode.set_initial_value(self.evl.z0,times[0])
        tend = times[0]
        t = [tend]
        z = [self.evl.z0.copy()]
        i = 0
        while self.ode.successful() and self.ode.t < times[-1]:
            if self.ode.t >= tend:
                i += 1
                tend = times[i]
            self.ode.integrate(tend)
            t.append(self.ode.t)
            z.append(self.ode.y)

        if outputs is None:
            outputs = self.outputs
        elif outputs.__class__ is "".__class__:
            outputs = (outputs,)
        res = self.compute_outputs(z, outputs=outputs)
        return (t,)+res

class VodeSimulatorStop(VodeSimulator):
    """
    Implements network simulation via scipy.integrate.
    Allows the specification of stop conditions on the simulation. Simulation stops at the
    first time point where a simulation result has been requested and the stop condition evaluates to True.
    """
    def __init__(self,net,times=[0, 1],x0=None,p=None,values={},evaluator=brneval.PythonBrnEvaluator,outputs=("species",),stopcondition=False):
        """
        arguments:        
        net - biochemical network as Brn object to simulate
        times - list of time points for which simulation results are desired
        x0 - initial conditions for network species
        p - parameter values
        evaluator - subclass of brneval.BrnEvaluator used to evaluate the ODE's rhs
        outputs - BRN variables to compute during simulation
        stopcondition - Boolean value, symbolic expression, or python function to determine preliminary simulation stops.
        """
        VodeSimulator.__init__(self,net,times,x0,p,values,evaluator,outputs)
        self.teststop = (lambda x: stopcondition) if type(stopcondition) is bool else stopcondition
        if type(stopcondition) is str:
            self.teststop = self.symbolic_stopcondition
            self.stopcondition = stopcondition
            self.expr_evl = brneval.ExpressionEvaluator(self.net, self.evl.p, self.evl.x0)

    def symbolic_stopcondition(self, x):
        """
        Returns true if simulation should be stopped for current state x, False otherwise.
        """
        self.expr_evl.set(x=x)
        return self.expr_evl.evalexpression(self.stopcondition)
        
    def run(self, times=None, outputs=None):
        """
        Method to solve the simulation task.
        Simulation data is generated at the time points given in the list 'times',
        and at default points if times=None.

        outputs - BRN variables to compute during simulation

        output:
        t - list of time points from simulation
        further outputs as specified in 'outputs', default is
        list of state values representing the trajectory
        """
        if times is None:
            times = self.times
        self.ode.set_initial_value(self.evl.z0,times[0])
        tend = times[0]
        t = [tend]
        z = [self.evl.z0.copy()]
        x = self.evl.convert_points([z[-1]])[0]
        i = 0
        while self.ode.successful() and self.ode.t < times[-1] and not self.teststop(x):
            if self.ode.t >= tend:
                i += 1
                tend = times[i]
            self.ode.integrate(tend)
            t.append(self.ode.t)
            z.append(self.ode.y)
            x = self.evl.convert_points([z[-1]])[0]

        if outputs is None:
            outputs = self.outputs
        elif outputs.__class__ is "".__class__:
            outputs = (outputs,)
        res = self.compute_outputs(z, outputs=outputs)
        return (t,)+res

class GillespieSimulator(Simulator):
    """
    implements network simulation with the Gillespie algorithm
    """
    def __init__(self,net,times=[0, 1],x0=None,p=None,values={},evaluator=brneval.PythonBrnEvaluator,outputs=("species",)):
        """
        arguments:        
        net - biochemical network as Brn object to simulate
        times - list of time points for which simulation results are desired
        x0 - initial conditions for network species
        p - parameter values
        evaluator - subclass of brneval.BrnEvaluator used to evaluate the ODE's rhs
        outputs - BRN variables to compute during simulation
        """
        Simulator.__init__(self,net,times,x0,p,evaluator=evaluator,outputs=outputs)

    def run(self, times=None, outputs=None):
        """
        Method to solve the simulation task.
        Simulation data is generated at the time points given in the list 'times',
        and at default points if times=None.

        outputs - BRN variables to compute during simulation

        output:
        t - list of time points from simulation
        further outputs as specified in 'outputs', default is
        list of state values representing the trajectory
        """
        # Code based on an implementation of the Gillespie algorithm by Timm Strecker, 2012
        if times is None:
            times = self.times
        tend = times[0]
        t = [tend]
        x = [self.evl.x0.copy()]
        i = 1
        while tend < times[-1]:
            a = self.net.eval(self.net.reactions, x=self.evl.x0)
            a_sum = np.sum(np.abs(a))
            a_ = np.cumsum(np.abs(a))
            rval = np.random.random(1)
            j = np.nonzero(a_/a_sum > rval)[0][0]   # first index where a_/a_sum > random is true
            assert(a[j] != 0)
            tau = np.random.exponential(1/a_sum,1) if a_sum > 0 else np.inf
            while i < len(times) and tend + tau >= times[i]:
                t.append(times[i])
                x.append(self.evl.x0.copy())
                i += 1
            tend += tau
            self.evl.set(x0=self.evl.x0 + np.sign(a[j]) * self.net.stoich[:,j])
        if outputs is None:
            outputs = self.outputs
        elif outputs.__class__ is "".__class__:
            outputs = (outputs,)
        res = self.compute_outputs(x, outputs=outputs)
        return (t,)+res

    def compute_outputs(self, xlist, outputs=("species",)):
        """
        from the list of species concentrations 'xlist', compute variables according
        to the sequence 'outputs'. Elements of 'outputs' may be 'species', 'reactions', 'variables',
        or named network elements.
        The outputs are returned as tuple in the order as they are given in 'outputs'.
        """
        outputvars = tuple([] for i in enumerate(outputs))
        if len(outputs)>1 or outputs[0] != "species":
            evl = brneval.ExpressionEvaluator(self.net, p=self.evl.p)
        for x in xlist:
            if len(outputs)>1 or outputs[0] != "species":
                evl.set(x=x)
            for o,ovar in zip(outputs,outputvars):
                if o == "species":
                    ovar.append(x)
                elif o == "reactions":
                    r = evl.evalexpression(self.net.reactions)
                    ovar.append(r)
                elif o == "variables":
                    v = evl.evalexpression(self.net.variables)
                    ovar.append(v)
                else:
                    n = evl.evalexpression(o)
                    ovar.append(n)
        return outputvars

class SundialsSimulator(Simulator):
    """
    implements network simulation via cython and the sundials C library
    """
    def __init__(self,net,times=[0,1],x0=None,p=None,values={},precompiled=None,filename=None,outputs=("species",),
                 noload=False,**kwargs):
        """
        arguments:        
        net - biochemical network as Brn object to simulate
        times - list of time points for which simulation results are desired
        x0 - initial conditions for network species
        p - parameter values
        values - dict to override selected values in network
        precompiled - name of previously compiled module for this network
        filename - use this name for module to compile
        outputs - BRN variables to compute during simulation
        noload - set to true if only compilation should be done
        """
        Simulator.__init__(self,net,np.asarray(times),x0,p,values,evaluator=brneval.BrnEvaluator,
                           outputs=outputs)
        if self.evl.havejac:
            self.ratejacobian = [[net.jacobian([r],[x],returntype='str')[0][0] for x in net.species]
                                 for r in net.reactions]
        if noload:
            # os.tempnam() on my Mac returns something like tmp.xxx, have to remove dots for valid modulname
            tempname = os.tempnam().replace(".", "_")
            self.do_compilation(tempname if filename is None else filename,**kwargs)
        else:
            self.compile(precompiled,filename,**kwargs)
            self.initialize()

    def initialize(self, x0=None, p=None, values={}):
        """
        set the initial condition for this simulation task to 'x0', a list or array of initial condition values.
        optionally also set the parameter values to 'p'
        """
        Simulator.initialize(self, x0, p, values)
        if 'cvode' in self.__dict__:
            self.cvode.initialize()
    
    def run(self, times=None, outputs=None):
        """
        Method to solve the simulation task.
        Simulation data is generated at the time points given in the list 'times',
        and at default points if times=None.

        outputs - BRN variables to compute during simulation

        output:
        t - list of time points from simulation
        further outputs as specified in 'outputs', default is
        list of state values representing the trajectory
        """
        if times is None:
            times = self.times
        self.cvode.set_initial_value(self.evl.z0,times[0])
        nextz = np.asarray([i for i in self.evl.z0],dtype=np.float64)
        tend = times[0]
        t = [tend]
        z = [self.evl.z0.copy()]
        i = 0
        while self.cvode.successful() and self.cvode.gett() < times[-1]:
            if self.cvode.gett() >= tend:
                i += 1
                tend = times[i]
            self.cvode.integrate(tend)
            t.append(self.cvode.gett())
            z.append(np.copy(self.cvode.getz(nextz)))

        if outputs is None:
            outputs = self.outputs
        elif outputs.__class__ is "".__class__:
            outputs = (outputs,)
        res = self.compute_outputs(z, outputs=outputs)
        return (t,)+res

    def do_compilation(self, fname, library_dirs=[], include_dirs=[]):
        """
        compile a sundials wrapper for this network and store in 'fname'
        (platform dependent suffix is added automatically)

        library_dirs - list of (non-standard) paths to search for sundials library
        include_dirs - list of (non-standard) paths to search for sundials include files

        intermediate build files are stored in the same directory as filename
        """
        if not have_cython:
            raise ImportError("Could not import Cython: " + cython_import_msg)
        dirname, modname = os.path.split(fname)
        pyxfilestr = self._template_str()
        # adding some lib / include dirs which may not be considered by default on all systems
        lib_dirs = library_dirs + ['/usr/local/lib']
        inc_dirs = include_dirs + ['/usr/local/include']
        setupstr = self._setup_template_str() % (modname, modname, lib_dirs.__repr__(), inc_dirs.__repr__())
        f = open(fname + ".pyx",'w')
        f.write(pyxfilestr)
        f.close()
        f = open(fname + "_setup.py",'w')
        f.write(setupstr)
        f.close()
        if len(dirname):
            cdir = os.getcwd()
            os.chdir(dirname)
        try:
            distutils.core.run_setup(modname + "_setup.py",script_args=['build_ext','--inplace'])
        finally:
            if len(dirname):
                os.chdir(cdir)
        
    def compile(self,precompiled=None,filename=None, **kwargs):
        """
        Compile a sundials wrapper for this network.
        
        If the same network has been compiled previously, give the full path
        (without extension) to the resulting module in the argument
        'precompiled', the compilation is then skipped.

        Optional keyword arguments are passed to ''do_compilation()''.

        Returns the full path to the resulting module (without extension).
        """
        if not precompiled:
            if filename is None:
                # os.tempnam() on my Mac returns something like tmp.xxx, have to remove dots for valid modulname
                fname = os.tempnam().replace(".", "_")
            else:
                fname = filename
            self.do_compilation(fname, **kwargs)
            dirname, modname = os.path.split(fname)
        else:
            dirname, modname = os.path.split(precompiled)
        f,p,d = imp.find_module(modname,[dirname])
        self.cvode_mod = imp.load_module(modname,f,p,d)
        self.cvode = self.cvode_mod.CVodeWrapper(self)
        return os.path.join(dirname,modname)

    def _template_str(self):
        defsstr = ""
        for i,s in enumerate(self.net.species):
            defsstr += "cdef double %s = ith(f_data.x,%d)" % (s,i)
            defsstr += '\n    '
        for i,p in enumerate(self.net.parameters):
            defsstr += "cdef double %s = ith(f_data.p,%d)" % (p,i)
            defsstr += '\n    '
        for v in self.net.variables:
            defsstr += "cdef double %s = %s" % (v,self.net.values[v])
            defsstr += '\n    '
        rateevalstr = ""
        for i,r in enumerate(self.net.reactions):
            rateevalstr += "setith(rate_nvector,%d,%s)" % (i, self.net.values[r])
            rateevalstr += '\n    '
        rjacevalstr = ""
        if self.evl.havejac:
            for j in xrange(len(self.net.species)):
                rjacevalstr += "next_rjac_column = rate_jacobian.data[%d]" % (j)
                rjacevalstr += '\n    '
                for i,r in enumerate(self.net.reactions):
                    if self.evl.have_scale:
                        rjacevalstr += ("next_rjac_column[%d] = (%s)*f_data.common_scale*ith(f_data.scale,%d)"
                                        % (i,self.ratejacobian[i][j],j) )
                    else:
                        rjacevalstr += "next_rjac_column[%d] = %s" % (i,self.ratejacobian[i][j])
                    rjacevalstr += '\n    '
        else:
            rjacevalstr = ""
        filestr = sundials_template_str() % {'defs':defsstr, 'rates':rateevalstr, 'rjac':rjacevalstr}
        return filestr

    def _setup_template_str(self):
        return sundials_setup_template_str()


class SundialsSimulatorStop(SundialsSimulator):
    """
    Implements network simulation via cython and the sundials C library.
    Allows the specification of stop conditions on the simulation. Simulation stops at the
    first time point where a simulation result has been requested and the stop condition evaluates to True.
    """
    def __init__(self,net,times=[0, 1],x0=None,p=None,values={},precompiled=None,filename=None,outputs=("species",),
                 library_dirs=[],noload=False,stopcondition="1.0"):
        """
        arguments:        
        net - biochemical network as Brn object to simulate
        times - list of time points for which simulation results are desired
        x0 - initial conditions for network species
        p - parameter values
        values - dict to override selected values in network
        precompiled - name of previously compiled module for this network
        filename - use this name for module to compile
        outputs - BRN variables to compute during simulation
        library_dirs - list of (non-standard) paths to search for sundials library
        noload - set to true if only compilation should be done
        stopcondition - Symbolic expression as string: simulation stops when expression evaluates to 0.
        """
        self.stopcondition = stopcondition
        SundialsSimulator.__init__(self,net,times,x0,p,values,precompiled,filename,outputs,library_dirs=library_dirs,noload=noload)

    def _template_str(self):
        defsstr = ""
        for i,s in enumerate(self.net.species):
            defsstr += "cdef double %s = ith(f_data.x,%d)" % (s,i)
            defsstr += '\n    '
        for i,p in enumerate(self.net.parameters):
            defsstr += "cdef double %s = ith(f_data.p,%d)" % (p,i)
            defsstr += '\n    '
        for v in self.net.variables:
            defsstr += "cdef double %s = %s" % (v,self.net.values[v])
            defsstr += '\n    '
        rateevalstr = ""
        for i,r in enumerate(self.net.reactions):
            rateevalstr += "setith(rate_nvector,%d,%s)" % (i, self.net.values[r])
            rateevalstr += '\n    '
        rjacevalstr = ""
        if self.evl.havejac:
            for j in xrange(len(self.net.species)):
                rjacevalstr += "next_rjac_column = rate_jacobian.data[%d]" % (j)
                rjacevalstr += '\n    '
                for i,r in enumerate(self.net.reactions):
                    if self.evl.have_scale:
                        rjacevalstr += ("next_rjac_column[%d] = (%s)*f_data.common_scale*ith(f_data.scale,%d)"
                                        % (i,self.ratejacobian[i][j],j) )
                    else:
                        rjacevalstr += "next_rjac_column[%d] = %s" % (i,self.ratejacobian[i][j])
                    rjacevalstr += '\n    '
        else:
            rjacevalstr = ""
        filestr = sundials_template_str(rootfinding=True) % {'defs':defsstr, 'rates':rateevalstr, 'rjac':rjacevalstr, 'stop':self.stopcondition}
        return filestr


def compile_model(net, modulename, builddir=".", **kwargs):
    """
    compile model 'net' to a shared library module for use with
    SundialsSimulator

    modulename - name of the shared library (potentially including suffix .so)
    builddir - directory where build files are placed in (default is current directory)

    Example:
    >>> compile_model(net, "net_module")
    >>> sim = SundialsSimulator(net, times=np.arange(10.0), precompiled="net_module")
    """
    f = os.path.join(builddir, modulename)
    SundialsSimulator(net, filename=f, noload=True, **kwargs)

def sundials_setup_template_str():
    return """# setup script for a pybrn compiled sundials wrapper
from distutils.core import setup
from distutils.extension import Extension
from Cython.Distutils import build_ext

ext_modules = [Extension("%s", ["%s.pyx"],libraries=["sundials_cvode","sundials_nvecserial","m"],
                         library_dirs=%s, include_dirs=%s)]

setup(
  cmdclass = {'build_ext': build_ext},
  ext_modules = ext_modules
)
"""

def sundials_template_str(rootfinding=False):
    return ("""import numpy as np
cimport numpy as np
cimport cython
from libc.math cimport *

# constants from cvode.h
#/* lmm */
DEF CV_ADAMS = 1
DEF CV_BDF   = 2
#/* iter */
DEF CV_FUNCTIONAL = 1
DEF CV_NEWTON     = 2
#/* itol */
DEF CV_SS = 1
DEF CV_SV = 2
DEF CV_WF = 3
#/* itask */
DEF CV_NORMAL         = 1
DEF CV_ONE_STEP       = 2
DEF CV_NORMAL_TSTOP   = 3
DEF CV_ONE_STEP_TSTOP = 4

#/*
#* CVODE return flags
#*/
DEF CV_SUCCESS               = 0
DEF CV_TSTOP_RETURN          = 1
DEF CV_ROOT_RETURN           = 2
DEF CV_WARNING              = 99
DEF CV_TOO_MUCH_WORK        = -1
DEF CV_TOO_MUCH_ACC         = -2
DEF CV_ERR_FAILURE          = -3
DEF CV_CONV_FAILURE         = -4
DEF CV_LINIT_FAIL           = -5
DEF CV_LSETUP_FAIL          = -6
DEF CV_LSOLVE_FAIL          = -7
DEF CV_RHSFUNC_FAIL         = -8
DEF CV_FIRST_RHSFUNC_ERR    = -9
DEF CV_REPTD_RHSFUNC_ERR    = -10
DEF CV_UNREC_RHSFUNC_ERR    = -11
DEF CV_RTFUNC_FAIL          = -12
DEF CV_MEM_FAIL             = -20
DEF CV_MEM_NULL             = -21
DEF CV_ILL_INPUT            = -22
DEF CV_NO_MALLOC            = -23
DEF CV_BAD_K                = -24
DEF CV_BAD_T                = -25
DEF CV_BAD_DKY              = -26

cdef extern from "stdio.h":
    cdef struct _IO_FILE:
        pass
    ctypedef _IO_FILE FILE

cdef extern from "sundials/sundials_types.h":
    ctypedef double realtype

cdef extern from "sundials/sundials_nvector.h":
    cdef struct _generic_N_Vector:
        void *content
    #ctypedef struct _generic_N_Vector _generic_N_Vector
    ctypedef _generic_N_Vector *N_Vector
    void N_VDestroy(N_Vector v)
    void N_VLinearSum(realtype a, N_Vector x, realtype b, N_Vector y, N_Vector z)
    void N_VConst(realtype c, N_Vector z)
    void N_VProd(N_Vector x, N_Vector y, N_Vector z)
    void N_VScale(realtype c, N_Vector x, N_Vector z)

cdef extern from "nvector/nvector_serial.h":
    cdef struct _N_VectorContent_Serial:
        int length
        realtype *data
    ctypedef _N_VectorContent_Serial *N_VectorContent_Serial
    N_Vector N_VNew_Serial(int vec_length)
    void N_VDestroy_Serial(N_Vector v)

  
cdef extern from "sundials/sundials_dense.h":
    ctypedef struct _DenseMat:
        int M
        int N
        double** data
    ctypedef _DenseMat *DenseMat
    DenseMat DenseAllocMat(long int M, long int N)
    void DenseZero(DenseMat A)
    void DenseFreeMat(DenseMat A)

cdef extern from "cvode/cvode.h":
    # type definitions from cvode.h
    ctypedef int (*CVRhsFn)(realtype t, N_Vector y,
                N_Vector ydot, void *f_data)
    ctypedef int (*CVRootFn)(realtype t, N_Vector y, realtype *gout,
                             void *g_data)
    
    void *CVodeCreate(int lmm, int iter)

    int CVodeSetFdata(void *cvode_mem, void *f_data)
    int CVodeSetMaxNumSteps(void *cvode_mem, long int mxsteps)
    int CVodeSetInitStep(void *cvode_mem, realtype hin)
    int CVodeSetMinStep(void *cvode_mem, realtype hmin)
    int CVodeSetMaxStep(void *cvode_mem, realtype hmax)
    int CVodeSetStopTime(void *cvode_mem, realtype tstop)
    int CVodeSetTolerances(void *cvode_mem,
                   int itol, realtype reltol, void *abstol)
    int CVodeMalloc(void *cvode_mem, CVRhsFn f, realtype t0, N_Vector y0,
            int itol, realtype reltol, void *abstol)
    int CVodeReInit(void *cvode_mem, CVRhsFn f, realtype t0, N_Vector y0,
            int itol, realtype reltol, void *abstol)
    int CVode(void *cvode_mem, realtype tout, N_Vector yout,
          realtype *tret, int itask)
    int CVodeGetCurrentTime(void *cvode_mem, realtype *tcur)
    int CVodeRootInit(void *cvode_mem, int nrtfn, CVRootFn g, void *g_data)
    void CVodeFree(void **cvode_mem)

cdef extern from "cvode/cvode_dense.h":
    ctypedef int (*CVDenseJacFn)(long int N, DenseMat J, realtype t,
                     N_Vector y, N_Vector fy, void *jac_data,
                     N_Vector tmp1, N_Vector tmp2, N_Vector tmp3)
    int CVDense(void *cvode_mem, int N)
    int CVDenseSetJacFn(void *cvode_mem, CVDenseJacFn djac, void *jac_data)
    void DensePrint(DenseMat A)

cdef inline double ith(N_Vector v, int i):
    return (<N_VectorContent_Serial>(v.content)).data[i]
 
cdef inline N_Vector setith(N_Vector v, int i, double val):
    (<N_VectorContent_Serial>(v.content)).data[i] = val
    return v

cdef inline int nvlength(N_Vector v):
    return (<N_VectorContent_Serial>(v.content)).length

def SundialsError(Exception):
    pass

cdef inline int check_flag(int flagvalue, char *funcname, int opt):
    # /* Check if SUNDIALS function returned NULL pointer - no memory allocated */
    if (opt == 0 and flagvalue is None):
        raise SundialsError(funcname + "() failed - returned NULL pointer");
        return(1);
    # /* Check if flag < 0 */
    elif (opt == 1):
        if (flagvalue < 0):
            raise SundialsError(funcname + "() failed with flag = " + str(flagvalue));
            return(1)
    # /* Check if function returned NULL pointer - no memory allocated */
    elif (opt == 2 and flagvalue is None):
        raise MemoryError(funcname + "() failed - returned NULL pointer");
        return(1);
    return(0);

@cython.boundscheck(False)
@cython.wraparound(False)
cdef void matvecmul(DenseMat mat, N_Vector x, N_Vector y):
    # compute y = mat * x with M times N matrix mat, where entries of mat are stored column-wise
    cdef int row, col
    cdef double *coli
    cdef double xi
    N_VConst(0,y)
    for col in range(mat.N):
        coli = mat.data[col]
        xi = ith(x,col)
        for row in range(mat.M):
            (<N_VectorContent_Serial>(y.content)).data[row] += coli[row]*xi

@cython.boundscheck(False)
@cython.wraparound(False)
cdef void matmatmul(DenseMat A, DenseMat B, DenseMat C):
    # compute C = A * B as matrix multiplication
    cdef int row, col, i
    cdef double *Ccoli
    cdef double *Bcoli
    cdef double res
    for col in range(C.N):
        Ccoli = C.data[col]
        for row in range(C.M):
            res = 0
            Bcoli = B.data[col]
            for k in range(A.N):
                res += A.data[k][row] * Bcoli[k]
            Ccoli[row] = res

cdef inline void ndarray2nvector(np.ndarray[np.float64_t,ndim=1] x, N_Vector y):
    # copy the contents of numpy array x into N_Vector y
    cdef int i
    if x.shape[0] != nvlength(y):
        raise IndexError("x and y must be of same length.")
    for i in range(nvlength(y)):
        setith(y,i,x[i])

cdef inline void ndarray2densemat(np.ndarray[np.float64_t,ndim=2] x, DenseMat y):
    # copy the contents of numpy array x into DenseMat y
    cdef int i, j
    if x.shape[0] != y.M or x.shape[1] != y.N:
        raise IndexError("x and y must be of same shape.")
    for i in range(y.M):
        for j in range(y.N):
            y.data[j][i] = x[i,j]

cdef struct _BrnFdata:
    N_Vector p
    N_Vector v
    N_Vector x
    N_Vector scale
    N_Vector Gxbar
    double common_scale
    DenseMat N
    DenseMat rjac
    DenseMat L
    bint have_conservation
    bint have_scale
ctypedef _BrnFdata *BrnFdata

cdef void computex(N_Vector z, BrnFdata f_data):
    cdef int i
    if f_data.have_conservation:
        # formula: x = L z + Gxbar
        matvecmul(f_data.L,z,f_data.x)
        N_VLinearSum(1,f_data.x,1,f_data.Gxbar,f_data.x)
    else:
        for i in range(nvlength(z)):
            setith(f_data.x,i,ith(z,i))
    if f_data.have_scale:
        N_VProd(f_data.x,f_data.scale,f_data.x)

cdef void computerates(BrnFdata f_data):
    %(defs)s
    cdef N_Vector rate_nvector = f_data.v
    # compute reaction rates
    %(rates)s
    if f_data.have_scale:
        N_VScale(f_data.common_scale,rate_nvector,rate_nvector)

cdef void computeratejac(BrnFdata f_data):
    %(defs)s
    cdef DenseMat rate_jacobian = (<BrnFdata>f_data).rjac
    cdef double *next_rjac_column
    # compute reaction rate jacobian
    %(rjac)s

cdef int fun(realtype t, N_Vector z, N_Vector zdot, void *f_data):
    cdef int i, j
    computex(z,<BrnFdata>f_data)
    computerates(<BrnFdata>f_data)
    matvecmul((<BrnFdata>f_data).N,(<BrnFdata>f_data).v,zdot)
    return CV_SUCCESS

cdef int jacfun(long int N, DenseMat J, realtype t, N_Vector z, N_Vector fz, void *f_data,
        N_Vector tmp1, N_Vector tmp2, N_Vector tmp3):
    cdef int i, j, k, l
    cdef double tempvar
    cdef DenseMat Nr = (<BrnFdata>f_data).N
    cdef DenseMat L = (<BrnFdata>f_data).L
    cdef DenseMat rjac = (<BrnFdata>f_data).rjac
    computex(z,<BrnFdata>f_data)
    computeratejac(<BrnFdata>f_data)
    # matrix multiplication for J
    # formula: jac = Nr * rjac * L
    if (<BrnFdata>f_data).have_conservation:
        for i in range(nvlength(z)):
            for j in range(nvlength(z)):
                J.data[j][i] = 0.
                for k in range(nvlength((<BrnFdata>f_data).x)):
                    tempvar = 0.
                    for l in range(nvlength((<BrnFdata>f_data).v)):
                        tempvar += Nr.data[l][i]*rjac.data[k][l]
                    J.data[j][i] += tempvar*L.data[j][k]
    else:
        matmatmul(Nr,rjac,J)
    return CV_SUCCESS
""" + ( """
cdef realtype stopcondition(BrnFdata f_data):
    %(defs)s
    return %(stop)s

cdef int root_fun(realtype t, N_Vector z, realtype *gout, void *g_data):
    computex(z,<BrnFdata>g_data)
    gout[0] = stopcondition(<BrnFdata>g_data)
    return 0"""
if rootfinding else "") + """
cdef class CVodeWrapper:
    cdef void *cvode_mem
    cdef int dimz, numparams, numreactions, numspecies
    cdef N_Vector z0
    cdef N_Vector abstol
    cdef N_Vector times
    cdef double reltol
    cdef _BrnFdata fdata
    cdef bint have_jac

    cdef int flag
    cdef realtype tout
    cdef N_Vector z

    cdef sim

    def __init__(self,simulator):
        cdef int i, j
        self.sim = simulator
        self.dimz = simulator.evl.z0.shape[0]
        self.numspecies = len(simulator.net.species)
        self.numparams = len(simulator.net.parameters)
        self.numreactions = len(simulator.net.reactions)
        self.have_jac = self.sim.evl.havejac

        self.z0 = N_VNew_Serial(self.dimz)
        self.z = N_VNew_Serial(self.dimz)
        ndarray2nvector(simulator.evl.z0,self.z0)
        self.abstol = N_VNew_Serial(self.dimz)
        N_VConst(1e-6, self.abstol)
        self.reltol = 1e-6
        self.times = N_VNew_Serial(len(simulator.times))
        ndarray2nvector(simulator.times,self.times)

        # fdata fields: x, v, p, N, scale, Gxbar, common_scale, rjac, L, have_jac, have_conservation, have_scale
        self.fdata.have_conservation = simulator.evl.have_conservation
        self.fdata.have_scale = simulator.evl.have_scale
        self.fdata.x = N_VNew_Serial(self.numspecies)

        N_VConst(0,self.fdata.x)
        self.fdata.p = N_VNew_Serial(self.numparams)
        ndarray2nvector(simulator.evl.p,self.fdata.p)
        self.fdata.v = N_VNew_Serial(self.numreactions)
        N_VConst(0,self.fdata.v)
        self.fdata.N = DenseAllocMat(self.dimz,self.numreactions)
        ndarray2densemat(simulator.evl.N,self.fdata.N)
        self.fdata.rjac = DenseAllocMat(self.numreactions,self.numspecies)
        DenseZero(self.fdata.rjac)
        if self.fdata.have_scale:
            self.fdata.scale = N_VNew_Serial(self.numspecies)
            N_VConst(0,self.fdata.scale)
        if self.fdata.have_conservation:
            self.fdata.L = DenseAllocMat(self.numspecies,self.dimz)
            ndarray2densemat(simulator.net.conserv.L,self.fdata.L)
            self.fdata.Gxbar = N_VNew_Serial(self.numspecies)

        cdef int flag
        self.cvode_mem = CVodeCreate(CV_BDF, CV_NEWTON)
        flag = CVodeSetFdata(self.cvode_mem, &self.fdata);
        check_flag(flag, "CVodeSetFData", 1)
        flag = CVodeMalloc(self.cvode_mem, &fun, 0, self.z0, CV_SV, self.reltol, self.abstol)
        check_flag(flag, "CVodeMalloc", 1)
        flag = CVDense(self.cvode_mem, self.dimz)
        check_flag(flag, "CVodeMalloc", 1)"""
+ ( """
        flag = CVodeRootInit(self.cvode_mem, 1, &root_fun, &self.fdata)
        check_flag(flag, "CVodeRootInit", 1)"""
if rootfinding else "") + """
        if self.have_jac:
            flag = CVDenseSetJacFn(self.cvode_mem, &jacfun, &self.fdata)
            check_flag(flag,"CVDenseSetJacFn",1)

    def __dealloc__(self):
        N_VDestroy(self.z0)
        N_VDestroy(self.z)
        N_VDestroy(self.abstol)
        N_VDestroy(self.times)
        N_VDestroy(self.fdata.v)
        N_VDestroy(self.fdata.p)
        N_VDestroy(self.fdata.x)
        DenseFreeMat(self.fdata.N)
        DenseFreeMat(self.fdata.rjac)
        if self.fdata.have_scale:
            N_VDestroy(self.fdata.scale)
        if self.fdata.have_conservation:
            N_VDestroy(self.fdata.Gxbar)
            DenseFreeMat(self.fdata.L)
        CVodeFree(&self.cvode_mem)

    def integrate(self,realtype tend):
        self.flag = CVode(self.cvode_mem, tend, self.z, &self.tout, CV_NORMAL)

    def gett(self):
        return self.tout

    def getz(self,np.ndarray[np.float64_t,ndim=1] z):
        if z.shape[0] != self.dimz:
            raise IndexError("z must be of length " + str(self.dimz))
        for i in range(self.dimz):
            z[i] = ith(self.z,i)
        return z

    def successful(self):
        return self.flag == CV_SUCCESS

    def set_initial_value(self,np.ndarray[np.float64_t,ndim=1] z0, realtype t0):
        ndarray2nvector(z0,self.z0)
        self.flag = CVodeReInit(self.cvode_mem, &fun, t0, self.z0, CV_SV, self.reltol, self.abstol);
        self.tout = t0

    def initialize(self):
        cdef np.ndarray[np.float64_t,ndim=1] scale, param
        cdef np.ndarray[np.float64_t,ndim=1] Gxbar
        cdef int i
        param = self.sim.evl.p
        ndarray2nvector(param,self.fdata.p)
        if self.fdata.have_scale:
            scale = np.asarray(self.sim.net.scale_fun()(param),dtype=float)
            self.fdata.common_scale = max(scale)
            ndarray2nvector(scale/self.fdata.common_scale,self.fdata.scale)
        else:
            self.fdata.common_scale = 1.
        if self.fdata.have_conservation:
            if self.fdata.have_scale:
                Gxbar = np.dot(self.sim.net.conserv.G,np.dot(self.sim.net.conserv.G.T,self.sim.evl.x0/self.sim.evl.scale))
            else:
                Gxbar = np.dot(self.sim.net.conserv.G,np.dot(self.sim.net.conserv.G.T,self.sim.evl.x0))
            ndarray2nvector(Gxbar,self.fdata.Gxbar)
""")
    

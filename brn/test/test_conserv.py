"""
brn.test.testbrn - unittest test suite for brn/brn.py
"""
# Copyright (C) 2010 Steffen Waldherr waldherr@ist.uni-stuttgart.de
# Time-stamp: <Last change 2010-11-12 09:44:01 by Steffen Waldherr>

import unittest
import numpy as np
import brn
from generic import makesimplenet, make_conservation_net

class TestConservationConsistency(unittest.TestCase):
    """
    Testing internal consistency of 
    """
    def setUp(self):
        self.net1 = make_conservation_net()
        self.net1.conservation_analysis()
        self.net2 = makesimplenet()
        self.net2.conservation_analysis()
    
    def test_conservation_simplenet(self):
        res1 = np.dot(self.net1.conserv.L,self.net1.conserv.Nr) - self.net1.stoich
        for i in res1.flat:
            self.assertAlmostEqual(i,0)
        LTG = np.dot(self.net1.conserv.L.T,self.net1.conserv.G)
        for i in LTG.flat:
            self.assertAlmostEqual(i,0)

    def test_conservation_conserved_net(self):
        res1 = np.dot(self.net2.conserv.L,self.net2.conserv.Nr) - self.net2.stoich
        for i in res1.flat:
            self.assertAlmostEqual(i,0)
        LTG = np.dot(self.net2.conserv.L.T,self.net2.conserv.G)
        for i in LTG.flat:
            self.assertAlmostEqual(i,0)

    def test_coordinate_trafos(self):
        x0 = [3,0,0]
        x2z_fun = self.net1.conserv.full2reduced_fun()
        z2x_fun = self.net1.conserv.reduced2full_fun(x0)
        res = z2x_fun(x2z_fun(x0)) - x0
        for i in res.flat:
            self.assertAlmostEqual(i,0)

def makesuite():
    return unittest.defaultTestLoader.loadTestsFromModule(__import__("test_conserv"))

if __name__ == "__main__":
    suite = makesuite()
    unittest.TextTestRunner(verbosity=2).run(suite)

"""
test_brncomp - unittest suite for module brn.brncomp
"""
# Copyright (C) 2010 Steffen Waldherr waldherr@ist.uni-stuttgart.de
# Time-stamp: <Last change 2012-09-27 19:49:17 by Steffen Waldherr>

import os
import math
import numpy as np
import unittest

import brn
import brn.brncomp as brncomp
import generic

class TestJacobian(unittest.TestCase):
    """
    Testing brncomp.jacobian method
    """
    def test_scalar_jacobian(self):
        res = brncomp.jacobian(['f'],['x'],vars=[['x'],['k']],valuedict={'f':'k*x**2'},returntype="str")
        self.assertEqual(res,[['2*k*x']])
        fun = brncomp.jacobian(['f'],['x'],vars=[['x'],['k']],valuedict={'f':'k*x**2'},returntype="fun")
        self.assertEqual(fun([2],[2]),[[8]])

    def test_vector_jacobian(self):
        res = brncomp.jacobian(['x*y'],['x','y'],returntype="str")
        self.assertEqual(res,[['y','x']])
        res = brncomp.jacobian(['x**2','x'],['x'],returntype="str")
        self.assertEqual(res,[['2*x'],['1']])

    def test_matrix_jacobian(self):
        vars = ['x','y']
        fun = ['x*y','x*y**2']
        res = brncomp.jacobian(fun,vars,returntype="str")
        self.assertEqual(res,[['y','x'],['pow(y,2)','2*x*y']])

    def test_value_substitution(self):
        res = brncomp.jacobian(['k*x**2'],['x'],valuedict={'k':2},returntype="str")
        self.assertEqual(res,[['4.0*x']])

    def test_2ndvalue_substitution(self):
        res = brncomp.jacobian(['k*x**2'],['x'],vars=[['x','k']],valuedict={'f':'k*g','g':'x**2'},
                               returntype="str")
        self.assertEqual(res,[['2*k*x']])
        res = brncomp.jacobian(['k*x**2'],['x'],vars=[['x'],['k']],valuedict={'f':'k*g','g':'x**2'},
                               returntype="fun")
        self.assertEqual(res([2],[3]),[[12]])

    def test_substitution_chain(self):
        res = brncomp.jacobian(['2*z'],['u'],valuedict={'z':'y', 'y':'x', 'x':'w', 'w':'v', 'v':'u'},returntype='str')
        self.assertEqual(res,[['2']])

    def test_jacobian_algebraic_loop(self):
        valuedict = {'z':'1-y', 'y':'x+z'}
        self.assertRaises(ValueError,brncomp.jacobian,['2*z'],['x'],valuedict=valuedict)

class TestBrnJacobian(unittest.TestCase):
    """
    Testing brn.Brn.jacobian method
    """
    def test_scalarnet(self):
        net = generic.scalarnet()
        jac = net.jacobian(net.reactions,net.species)
        self.assertEqual(jac([100],[]),[[1.]])

    def test_simplenet(self):
        net = generic.makesimplenet()
        jac = net.jacobian(net.reactions,net.parameters,returntype="str")
        self.assertEqual(jac,[['0', '0'], ['A', '0'], ['0', 'B']])
        jac = net.jacobian(net.reactions,net.parameters,returntype="fun")
        self.assertEqual(jac([1,1],[2,2]), [[0, 0], [1, 0], [0, 1]])

    def test_mathexpr_jacobian(self):
        net = generic.make_mathexpr_net()
        jac = net.jacobian(net.reactions,net.species)
        self.assertEqual(jac([1],[1, 1]),[[-math.exp(-1)], [2]])

class TestLinearapproximation(unittest.TestCase):
    """
    Testing brn.brncomp.linearpproximation method
    """
    def test_scalarnet(self):
        net = generic.scalarnet()
        Afun,Bfun,Cfun = brncomp.linearapproximation(net, returntype="fun")
        self.assertEqual(Afun([100],[]),[[-1.]])
        self.assertEqual(Bfun,None)
        self.assertEqual(Cfun,None)
        A,B,C = brncomp.linearapproximation(net, returntype="num")
        self.assertEqual(A,[[-1.]])
        self.assertEqual(B,None)
        self.assertEqual(C,None)

    def test_scaled_net(self):
        net = generic.make_scaled_net()
        Afun,Bfun,Cfun = brncomp.linearapproximation(net, returntype="fun")
        Atrue = np.asarray([[-1.,0],[0,-1.]])
        Atest = Afun([1, 1],[1, 2])
        self.assertTrue(np.alltrue(Atrue-Atest <= 1e-8))
        A,B,C = brncomp.linearapproximation(net, x=[1,1], p=[1,2], returntype="num")
        self.assertTrue(np.alltrue(Atrue-A <= 1e-8))

    def test_conservation_net(self):
        net = generic.make_conservation_net()
        Afun,Bfun,Cfun = brncomp.linearapproximation(net, returntype="fun")
        Atrue = np.dot(np.dot(net.conserv.Nr,np.asarray([[1.,0,0],[0,1,0]])),net.conserv.L)
        Atest = Afun([1, 1, 1],[1, 1])
        self.assertTrue(np.alltrue(Atrue-Atest <= 1e-8))
        A,B,C = brncomp.linearapproximation(net, x=[1,1,1], p=[1,1], returntype="num")
        self.assertTrue(np.alltrue(Atrue-A <= 1e-8))

    def test_inputoutput(self):
        net = generic.makesimplenet()
        inputs = ['k3']
        outputs = ['B', 'cos(C)']
        Afun,Bfun,Cfun = brncomp.linearapproximation(net, inputs=inputs, outputs=outputs, returntype="fun")
        x = np.asarray([1,1])
        p = net.eval(net.parameters)
        Atest = Afun(x,p)
        Btest = Bfun(x,p)
        Ctest = Cfun(x,p)
        Atrue = np.asarray([[-p[0],0],[p[0],-p[1]]])
        Btrue = np.asarray([[0],[-1.]])
        Ctrue = np.asarray([[0, 1.],[0, -np.sin(1)]])
        self.assertTrue(np.alltrue(Atrue-Atest <= 1e-8))
        self.assertTrue(np.alltrue(Btrue-Btest <= 1e-8))
        self.assertTrue(np.alltrue(Ctrue-Ctest <= 1e-8))

    def test_inputoutput_conserv(self):
        net = generic.make_conservation_net()
        inputs = ['k2']
        outputs = ['x2']
        x = np.asarray([1,1,1])
        p = np.asarray([1,1])
        A,B,C = brncomp.linearapproximation(net, x, p, inputs=inputs, outputs=outputs, returntype="num")
        Atrue = np.dot(np.dot(net.conserv.Nr,np.asarray([[1.,0,0],[0,1,0]])),net.conserv.L)
        self.assertTrue(np.alltrue(Atrue-A <= 1e-8))
        Btrue = np.dot(net.conserv.Nr,np.asarray([[0],[1.]]))
        self.assertTrue(np.alltrue(Btrue-B <= 1e-8))
        Ctrue = np.dot(np.asarray([[0, 0, 1.]]), net.conserv.L)
        self.assertTrue(np.alltrue(Ctrue-C <= 1e-8))

class TestBrnLinearapproximation(unittest.TestCase):
    """
    Testing brn.Brn.linearpproximation method
    """
    def test_simplenet(self):
        net = generic.makesimplenet()
        res = net.linearapproximation()
        res = net.linearapproximation(values={'A':2.0, 'k2': 3.0})
        res = net.linearapproximation(values={'A':2.0, 'k2': 3.0}, x=[1,1])
        res = net.linearapproximation(values={'A':2.0, 'k2': 3.0}, p=[3,3])

class TestVariableSubstitution(unittest.TestCase):
    """
    Testing brn.brncomp.variable_substitution method.
    """
    def test_variable_substitution(self):
        vardict = {'v1':'k*v2', 'v2':'k**2'}
        resdict = brncomp.variable_substitution(vardict, ['k'])
        self.assertEqual(resdict['v1'], 'k**3')
        self.assertEqual(resdict['v2'], 'k**2')

def makesuite():
    return unittest.defaultTestLoader.loadTestsFromModule(__import__("test_brncomp"))


if __name__ == "__main__":
    suite = makesuite()
    unittest.TextTestRunner(verbosity=2).run(suite)


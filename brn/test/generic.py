"""
brn.test.generic: generic utilities for test code

Copyright (C) 2010 Steffen Waldherr waldherr@ist.uni-stuttgart.de
"""

import numpy as np
import brn

def makesimplenet():
    """
    the chain -> A -> B -> with constant / linear reaction rates
    and a variable C = B
    """
    stoich = np.asarray([[1,-1,0],[0,1,-1]])
    species = ['A','B']
    reactions = ['v1','v2','v3']
    parameters = ['k2','k3']
    variables = ['C']
    net = brn.Brn(stoich,species,reactions,parameters,variables)
    net.set({'v1':1, 'v2':'k2*A', 'v3':'k3*C', 'C':'B', 'k2':1, 'k3':1})
    return net

def make_conservation_net():
    """
    the network x0 -> x1 -> x2 with linear reaction rates
    """
    stoich = [[-1,0],[1,-1],[0,1]]
    parameters = ['k1','k2']
    net = brn.Brn(stoich,parameters=parameters)
    net.set({'v0':'k1*x0', 'v1':'k2*x1', 'k1':1, 'k2':1})
    return net

def make_nonlinear_scalar_net():
    """
    xdot = -k + x**2
    """
    stoich = [[-1, 1]]
    parameters = ['k']
    net = brn.Brn(stoich,['x'],['v1','v2'],parameters=parameters)
    net.set({'k':1., 'v1':'k', 'v2':'pow(x,2)'})
    return net

def make_scaled_net():
    """
    a simple network where the species are scaled differentially
    x1dot = (1/k1) k1 x1
    x2dot = (1/k2) k2 x2
    """
    stoich = [[1, 0],[0, 1]]
    species = ['x1','x2']
    reactions = ['v1','v2']
    parameters = ['k1','k2']
    net = brn.Brn(stoich,species,reactions,parameters,scale=['1/k1','1/k2'])
    net.set({'v1':'-k1*x1', 'v2':'-k2*x2', 'k1':1., 'k2':2.})
    return net
    
def adaptnet():
    """
    not yet complete !!!
    network with adaptation
    -> a ->
    -> m ->
    """
    stoich = np.asarray([[1,-1,0,0],[0,0,1,-1]])
    species = ['a','m']
    reactions = ['v1','v2','v3','v4']
    parameters = ['u','k2','k3','k4']
    net = brn.Brn(stoich,species,reactions,parameters,[])

def scalarnet():
    """
    xdot = (-1) * x
    """
    return brn.Brn([[-1]],['x'],['v'],rates={'v':'x'})

def make_mathexpr_net():
    """
    a test network with expressions from math module
    """
    rates = {'v1':'gamma*exp(-x1)', 'v2': 'x1**(2*k1)'}
    ics = {'x1':'sin(k1)'}
    parvals = {'k1':1., 'gamma':1.}
    return brn.Brn([[1,-1]],['x1'],['v1','v2'],['k1','gamma'],rates=rates,ics=ics,parvals=parvals)

def make_variables_net():
    """
    A network with badly sorted variables.
    """
    stoich = np.asarray([[1,-1,0],[0,1,-1]])
    species = ['A','B']
    reactions = ['v1','v2','v3']
    parameters = ['k2','k3']
    variables = ['r2', 'r3', 'C']
    net = brn.Brn(stoich,species,reactions,parameters,variables)
    net.set({'v1':1, 'v2':'r2', 'v3':'r3', 'C':'B', 'k2':1, 'k3':1, 'r2':'k2*A', 'r3':'k3*C', 'A':1, 'B':1})
    return net

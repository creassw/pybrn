"""
test_steadystate - test steady state computation in brn package
"""
# Copyright (C) 2010 Steffen Waldherr waldherr@ist.uni-stuttgart.de
# Time-stamp: <Last change 2010-11-17 22:32:57 by Steffen Waldherr>

import numpy as np
import unittest
import brn.steadystate as steadystate
import brn, brn.brncomp
import generic

class TestBrnSteadystate(unittest.TestCase):
    """
    Testing steady state computation from Brn
    """
    def setUp(self):
        self.simplenet = generic.makesimplenet()
        self.conservnet = generic.make_conservation_net()
        self.nlnet = generic.make_nonlinear_scalar_net()
        
    def test_steadystate_default(self):
        xs = self.simplenet.steadystate()
        for i in xs:
            self.assertAlmostEqual(i,1)

    def test_steadystate_customparam(self):
        xs = self.simplenet.steadystate(p=[2,2])
        for i in xs:
            self.assertAlmostEqual(i,0.5)
        xs = self.simplenet.steadystate(values={'k3':2})
        self.assertAlmostEqual(xs[0],1)
        self.assertAlmostEqual(xs[1],0.5)

    def test_steadysteate_with_conservation(self):
        x0 = [3,0,0]
        res = self.conservnet.steadystate(x0=x0) - np.asarray([0,0,3])
        for i in res:
            self.assertAlmostEqual(i,0)

    def test_steadystate_no_jacobian(self):
        oldvalue = brn.brncomp.have_sympy
        brn.brncomp.have_sympy = False
        try:
            xs = self.simplenet.steadystate()
            for i in xs:
                self.assertAlmostEqual(i,1)
        finally:
            brn.brncomp.have_sympy = oldvalue

    def test_steadystate_nonlinear(self):
        xs = self.nlnet.steadystate()
        self.assertAlmostEqual(xs[0]**2,self.nlnet.values['k'])


class TestSteadystateBranch(unittest.TestCase):
    """
    testing steadystate.steadystatebranch()
    """
    def setUp(self):
        self.simplenet = generic.makesimplenet()
        self.simplenet.set({'k2':1, 'k3':1})

    def test_ssbranch_default(self):
        pnominal = self.simplenet.eval(self.simplenet.parameters)
        x0 = self.simplenet.eval(self.simplenet.species)
        pb, xsb, info = steadystate.steadystate_branch(self.simplenet,'k3',1,10,0.1,pnominal,x0)
        for p,x in zip(pb,xsb):
            self.assertAlmostEqual(x[1], 1./p)
    
    def test_ssbranch_negative_dir(self):
        pnominal = self.simplenet.eval(self.simplenet.parameters)
        x0 = self.simplenet.eval(self.simplenet.species)
        pb, xsb, info = steadystate.steadystate_branch(self.simplenet,'k3',10,1,-0.1,pnominal,x0)
        for p,x in zip(pb,xsb):
            self.assertAlmostEqual(x[1], 1./p)

class TestBrnContinuation(unittest.TestCase):
    def test_brnssbranch_default(self):
        simplenet = generic.makesimplenet()
        simplenet.set({'k2':1, 'k3':1})
        p,x = simplenet.steadystate_branch('k3',1,10)
        for i,j in zip(p,x):
            self.assertAlmostEqual(j[0],1)
            self.assertAlmostEqual(j[1],1./i)

    def test_brnssbranch_conservation(self):
        net = generic.make_conservation_net()
        net.set({'x0':3})
        p,x = net.steadystate_branch('k1',1,10)
        for i,j in zip(p,x):
            self.assertAlmostEqual(np.sum(j),3)

def makesuite():
    return unittest.defaultTestLoader.loadTestsFromModule(__import__("test_steadystate"))

if __name__ == "__main__":
    suite = makesuite()
    unittest.TextTestRunner(verbosity=2).run(suite)


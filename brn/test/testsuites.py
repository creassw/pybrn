"""
testsuites - generate different unittest suites for brn package

"""
# Copyright (C) 2010 Steffen Waldherr waldherr@ist.uni-stuttgart.de
# Time-stamp: <Last change 2011-04-26 21:11:11 by Steffen Waldherr>

import unittest
import brn.test.test_brn, brn.test.test_mathutils, brn.test.test_brncomp, brn.test.test_conserv
import brn.test.test_steadystate, brn.test.test_simulation
import brn.test.test_sbmlimport
import brn.test.test_brn_evaluator, brn.test.test_cython

minreqmodules = [brn.test.test_brn,brn.test.test_mathutils,brn.test.test_conserv,
                 brn.test.test_brn_evaluator]
sympymodules = [brn.test.test_brncomp]
scipymodules = [brn.test.test_steadystate, brn.test.test_simulation]
sbmlmodules = [brn.test.test_sbmlimport]
compilemodules = [brn.test.test_cython]

def makesuite_minimal_reqs():
    return unittest.TestSuite([unittest.defaultTestLoader.loadTestsFromModule(t) for t in minreqmodules])

def makesuite_recommended_reqs():
    return unittest.TestSuite([unittest.defaultTestLoader.loadTestsFromModule(t) for t in
                               sympymodules+scipymodules+sbmlmodules])

def makesuite_all():
    modulelist = minreqmodules+sympymodules+scipymodules+sbmlmodules+compilemodules
    return unittest.TestSuite([unittest.defaultTestLoader.loadTestsFromModule(t) for t in modulelist])

def makesuite_compile():
    return unittest.TestSuite([unittest.defaultTestLoader.loadTestsFromModule(t) for t in compilemodules])

def runtest_minimal_reqs(verbosity=2):
    unittest.TextTestRunner(verbosity=verbosity).run(makesuite_minimal_reqs())

def runtest_recommended_reqs(verbosity=2):
    unittest.TextTestRunner(verbosity=verbosity).run(makesuite_recommended_reqs())

def runtest_compile(verbosity=2):
    unittest.TextTestRunner(verbosity=verbosity).run(makesuite_compile())

def runtest_all(verbosity=2):
    unittest.TextTestRunner(verbosity=verbosity).run(makesuite_all())


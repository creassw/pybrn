"""
brn.test.testbrn - unittest test suite for brn/brn.py

Copyright (C) 2010 Steffen Waldherr waldherr@ist.uni-stuttgart.de
"""

import os
import brn
import brn.brncomp
import brn.steadystate, brn.simulation
import unittest
import math
import numpy as np
import generic
import random

class TestSimpleBrn(unittest.TestCase):
    """
    Testing construction of the linear BRN -> A -> B -> 
    """
    def setUp(self):
        self.net = generic.makesimplenet()

    def test_string(self):
        netstr = str(self.net)
        print netstr
    
    def test_set(self):
        self.net.set({'v1':1.,'v2':'k2*A','v3':'k3*B'})
        self.assertEqual(1.,self.net.values['v1'])
        self.assertEqual('k2*A',self.net.values['v2'])
        self.assertEqual('k3*B',self.net.values['v3'])

        self.net.set({'A':1.,'B':2.})
        self.assertEqual(1.,self.net.values['A'])
        self.assertEqual(2.,self.net.values['B'])

        self.net.set({'k2':1.,'k3':1.})
        self.assertEqual(1.,self.net.values['k2'])
        self.assertEqual(1.,self.net.values['k3'])

        self.net.set({'C':'A+B'})
        self.assertEqual('A+B',self.net.values['C'])
    
    def test_eval(self):
        self.net.set(dict(zip(self.net.parameters,[1.,2.])))
        # test scalar evaluation
        res = self.net.eval('k2*k3') - 2
        self.assertAlmostEqual(res,0)
        # test vector evaluation
        res = self.net.eval(['k2*k3','k2-k3']) - np.asarray([2,-1])
        for r in res:
            self.assertAlmostEqual(r,0)
        # test matrix evaluation
        res = self.net.eval([['k2*k3','k2-k3'],['k2+k3','1']]) - np.asarray([[2,-1],[3,1]])
        for row in res:
            for r in row:
                self.assertAlmostEqual(r,0)

    def test_showreact(self):
        strlist = ['v1:  -> 1*A; rate: 1', 'v2: 1*A -> 1*B; rate: k2*A', 'v3: 1*B -> ; rate: k3*C']
        for i,v in enumerate(self.net.reactions):
            self.assertEqual(self.net.showreact(v),strlist[i])

    def test_evalparam(self):
        pvals = self.net.evalparam(self.net.parameters)
        for p,p0 in zip(pvals, (self.net.values[i] for i in self.net.parameters)):
            self.assertEqual(p,p0)
        self.assertEqual(self.net.evalparam("sin(k2+k3)"),math.sin(2))
        self.assertEqual(self.net.evalparam("sin(k2+k3)",values={'k2':2.}),math.sin(3))
        self.assertEqual(self.net.evalparam("sin(k2+k3)",p=[2., 2.]),math.sin(4))
        self.assertEqual(self.net.evalparam("k2+k3",p=[2., 2.],values={'k3':3.}),5)


class TestScalarNet(unittest.TestCase):
    """
    run all Brn methods on simplistic scalar net.
    """
    def setUp(self):
        self.net = generic.scalarnet()
    
    def test_conservation_analysis(self):
        self.net.conservation_analysis()
        self.assertEqual(np.dot(self.net.conserv.L,self.net.conserv.Nr),self.net.stoich)
    
    def test_eval(self):
        self.assertEqual(self.net.eval(self.net.species),0.)
        self.assertEqual(self.net.eval(self.net.reactions,values={'x':1.}),1.)
        self.assertEqual(self.net.eval("2.*x**3",values={'x':2.}),16.)
        self.assertEqual(self.net.eval("2.*x**3",x=[1.]),2.)
        self.assertEqual(self.net.eval("2.*x**3",x=[1.],values={'x':2.}),16.)

    def test_evalparam(self):
        # test looks strange because there are no parameters in generic.scalarnet()
        self.assertEqual(self.net.evalparam("0"),0.)

    def test_has_conservation(self):
        self.assertFalse(self.net.has_conservation)

    def test_has_scale(self):
        self.assertFalse(self.net.has_scale)

    def test_jacobian(self):
        if brn.brncomp.have_sympy:
            self.assertEqual(self.net.jacobian(self.net.reactions,self.net.species)([0],[]),[[1]])
        else:
            self.assertRaises(ImportError,self.net.jacobian,self.net.reactions,self.net.species)

    def test_rate_fun(self):
        fun = self.net.rate_fun()
        self.assertEqual(fun([1],[]),[1])
        self.assertEqual(fun([2.5],[]),[2.5])

    def test_scale_fun(self):
        fun = self.net.scale_fun()
        self.assertEqual(fun([]),[1])

    def test_set(self):
        self.net.set({'x':1.})
        self.assertEqual(self.net.values['x'],1.)

    def test_showreact(self):
        self.assertEqual(self.net.showreact('v',printstr=False),'v: 1*x -> ; rate: x')

    def test_simulate(self):
        if brn.simulation.have_scipy:
            t,x = self.net.simulate([0,1,2],values={'x':1})
            for ti,xi in zip(t,x):
                self.assertAlmostEqual(xi,np.e**(-ti),places=4)
        else:
            self.assertRaises(ImportError,self.net.simulate,[0,1,2])

    def test_steadystate(self):
        if brn.steadystate.have_scipy:
            self.assertEqual(self.net.steadystate(),[0])
        else:
            self.assertRaises(ImportError,self.net.steadystate)

class TestRateFunGeneration(unittest.TestCase):
    """
    Test method Brn.rhs_fun
    """
    def setUp(self):
        self.net = generic.makesimplenet()

    def test_simplenet(self):
        fun = self.net.rate_fun()
        self.assertTrue(np.all(fun([0,0],[0,0])==[1,0,0]))

    def test_integer_division(self):
        net = brn.Brn([1])
        net.set({"v0":"1 / 2 * x0"})
        v0 = net.rate_fun()([1],[])
        self.assertEqual(v0[0], 0.5)

class TestScaleFactor(unittest.TestCase):
    """
    test usage of scaling factor in networks
    """
    def setUp(self):
        self.netlist = [brn.Brn(1,'x','v',rates={'v':'2-x'}),
                        brn.Brn(1,'x','v',rates={'v':'2-x'},scale=0.5),
                        brn.Brn(1,'x','v',rates={'v':'2-x'},parameters=['a'],parvals={'a':2},scale='a'),
                        brn.Brn(1,'x','v',rates={'v':'2-x'},variables=['a'],varvals={'a':'2'},scale='a'),
                        ]
        
    def test_scale_fun(self):
        funs = [n.scale_fun() for n in self.netlist]
        scales = [f([2]) for f in funs]
        self.assertEqual(scales,[[1],[0.5],[2],[2]])

    def test_scale_property(self):
        self.assertEqual([n.has_scale for n in self.netlist],[False,True,True,True])

class TestConservationProperty(unittest.TestCase):
    """
    test the Brn.has_conservation property
    """
    def setUp(self):
        self.net1 = generic.makesimplenet()
        self.net2 = generic.make_conservation_net()

    def test_conservation_property_simplenet(self):
        self.assertFalse(self.net1.has_conservation)

    def test_conservation_property_conservationnet(self):
        self.assertTrue(self.net2.has_conservation)

class TestEval(unittest.TestCase):
    """
    test the Brn.eval method
    """
    def setUp(self):
        stoich = np.eye(2)
        species = ['s1','s2']
        reactions = ['r1','r2']
        parameters = ['p1','p2']
        variables = ['v1','v2']
        self.net = brn.Brn(stoich,species,reactions,parameters,variables)

    def test_precedence(self):
        self.assertEqual(self.net.eval('p1*p2',p=[1.0, 2.0],x=[3.0, 4.0]),2.0)
        self.assertEqual(self.net.eval('p1*p2',p=[1.0, 2.0],values={'p1':3.0}),6.0)

    def test_brn_eval_allnumerical(self):
        values = dict()
        for i in self.net.values:
            values[i] = random.random()
        self.net.set(values)
        res = self.net.eval(values.keys())
        self.assertEqual(list(res),values.values())
    
    def test_brn_eval_varsymbolic(self):
        values = dict(zip(self.net.species+self.net.reactions+self.net.parameters+self.net.variables,
                 [1,1]+[2,2]+[3,3]+['p1*s1','p2*r2']))
        self.net.set(values)
        res = self.net.eval(self.net.variables) 
        self.assertEqual(list(res),[3,6])
    
    def test_brn_eval_ratesymbolic(self):
        values = dict(zip(self.net.species+self.net.reactions+self.net.parameters,
                 [1,1]+['p1*s1','p2*s2']+[3,4]))
        self.net.set(values)
        res = self.net.eval(self.net.reactions) 
        self.assertEqual(list(res),[3,4])

    def test_brn_eval_allsymbolic(self):
        values = dict(zip(self.net.species+self.net.reactions+self.net.parameters+self.net.variables,
                 ['p1','p2']+['p1*s1','p2*s2']+[2,'3*p1']+['s1+s2','p1*s2']))
        self.net.set(values)
        res = self.net.eval(self.net.species+self.net.reactions+self.net.parameters+self.net.variables)
        self.assertEqual(list(res),[2,6,4,36,2,6,8,12])

    def test_brn_eval_mathexpr(self):
        net = generic.make_mathexpr_net()
        l = net.eval(net.parameters+net.species+net.reactions)
        for i,j in zip(l,[1, 1, math.sin(1), math.exp(-math.sin(1)), math.sin(1)**2]):
            self.assertAlmostEqual(i,j)
        f = net.rate_fun()
        for i,j in zip(f([2],[1,1]),[math.exp(-2), 2**2]):
            self.assertAlmostEqual(i,j)

class TestAddelements(unittest.TestCase):
    """
    test the Brn.addelements method
    """
    def setUp(self):
        self.netlist = [generic.makesimplenet(), generic.make_conservation_net(),
                        generic.make_scaled_net(), generic.scalarnet(),
                        generic.make_mathexpr_net()]

    def test_brn_addelements_species(self):
        for net in self.netlist:
            n = len(net.species)
            net.addelements(species='a1', values={'a1':1.0})
            self.assertEqual(len(net.species),n+1)
            self.assertTrue('a1' in net.species)
            self.assertEqual(net.eval('a1'), 1.0)
            net.addelements(species=['a2','a3'], values={'a2':1.0, 'a3':2.0})
            self.assertEqual(len(net.species),n+3)
            self.assertTrue('a2' in net.species)
            self.assertEqual(net.eval('a2'), 1.0)
            self.assertTrue('a3' in net.species)
            self.assertEqual(net.eval('a3'), 2.0)
    
    def test_brn_addelements_parameters(self):
        for net in self.netlist:
            n = len(net.parameters)
            net.addelements(parameters='a1', values={'a1':1.0})
            self.assertEqual(len(net.parameters),n+1)
            self.assertTrue('a1' in net.parameters)
            self.assertEqual(net.eval('a1'), 1.0)
            net.addelements(parameters=['a2','a3'], values={'a2':1.0, 'a3':2.0})
            self.assertEqual(len(net.parameters),n+3)
            self.assertTrue('a2' in net.parameters)
            self.assertEqual(net.eval('a2'), 1.0)
            self.assertTrue('a3' in net.parameters)
            self.assertEqual(net.eval('a3'), 2.0)

    def test_brn_addelements_reactions(self):
        for net in self.netlist:
            m = len(net.reactions)
            stoich = {'deg':{net.species[0]:-1}, 'prod':{net.species[0]:1}}
            net.addelements(reactions=["deg","prod"], stoich=stoich)
            self.assertEqual(len(net.reactions),m+2)
            self.assertEqual(net.stoich[0,net.reactions.index("deg")], -1)
            self.assertEqual(net.stoich[0,net.reactions.index("prod")], 1)

    def test_brn_addelements_speciesreactions(self):
        for net in self.netlist:
            m = len(net.reactions)
            n = len(net.species)
            stoich = {'conv':{"a1":-1, "a2":1}}
            net.addelements(reactions="conv", species=["a1", "a2"], stoich=stoich)
            self.assertEqual(len(net.reactions),m+1)
            self.assertEqual(len(net.species),n+2)
            self.assertEqual(net.stoich[net.species.index("a1"),net.reactions.index("conv")], -1)
            self.assertEqual(net.stoich[net.species.index("a2"),net.reactions.index("conv")], 1)

class TestMakeMassaction(unittest.TestCase):
    """
    test the make_massaction_network() method
    """
    def test_massaction_2d(self):
        stoich = [[-2,2],[1,-1]]
        n = brn.make_massaction_network(stoich)
        self.assertEqual(n.values["v1"], "k1*x1**2")
        self.assertEqual(n.values["v2"], "k2*x2")
        n = brn.make_massaction_network(stoich, species=["A", "B"], reactions=["ra", "rb"], parameters=["p1", "p2"],
                                        paramvalues=[1,1], ics=[2,0])
        self.assertEqual(n.values["ra"], "p1*A**2")
        self.assertEqual(n.values["rb"], "p2*B")
        self.assertEqual(n.values["A"], 2)
        self.assertEqual(n.values["B"], 0)
        self.assertEqual(n.values["p1"], 1)
        self.assertEqual(n.values["p2"], 1)

    def test_massaction_errors(self):
        stoich = [[-2,2],[1,-1]]
        self.assertRaises(ValueError, lambda: brn.make_massaction_network(stoich, species=["A", "B", "C"]))
        self.assertRaises(ValueError, lambda: brn.make_massaction_network(stoich, reactions=["A", "B", "C"]))
        self.assertRaises(ValueError, lambda: brn.make_massaction_network(stoich, parameters=["A", "B", "C"]))
        
def makesuite():
    return unittest.defaultTestLoader.loadTestsFromModule(__import__("test_brn"))

if __name__ == "__main__":
    suite = makesuite()
    unittest.TextTestRunner(verbosity=2).run(suite)

"""
test_simulation - test simulation in brn package
"""
# Copyright (C) 2010 Steffen Waldherr waldherr@ist.uni-stuttgart.de
# Time-stamp: <Last change 2012-10-12 12:47:23 by Steffen Waldherr>

import numpy as np
import math
import unittest
import brn.simulation as simulation
import brn, brn.brncomp
import generic
from brn import mathutils

def makesuite():
    return unittest.defaultTestLoader.loadTestsFromModule(__import__("test_simulation"))

if __name__ == "__main__":
    suite = makesuite()
    unittest.TextTestRunner(verbosity=2).run(suite)

class TestSimulator(unittest.TestCase):
    """
    Testing the Simulator abstract superclass
    """
    def setUp(self):
        self.simplenet = generic.makesimplenet()
        x0 = [self.simplenet.values[i] for i in self.simplenet.species]
        p = [self.simplenet.values[i] for i in self.simplenet.parameters]
        self.simsimple = simulation.Simulator(self.simplenet,1,x0,p)
        self.conservnet = generic.make_conservation_net()
        x0 = [self.conservnet.values[i] for i in self.conservnet.species]
        p = [self.conservnet.values[i] for i in self.conservnet.parameters]
        self.simconserv = simulation.Simulator(self.conservnet,1,x0,p)
        self.nlnet = generic.make_nonlinear_scalar_net()
        
    def test_simulator_init(self):
        x0 = [self.simplenet.values[i] for i in self.simplenet.species]
        self.simsimple.initialize(x0)
        for i in xrange(len(x0)):
            self.assertEqual(self.simsimple.evl.z0[i],x0[i])
        x0 = [3,0,0]
        self.simconserv.initialize(x0)
        for i in xrange(len(x0)):
            self.assertAlmostEqual(self.simconserv.evl.z2x(self.simconserv.evl.z0)[i],x0[i])

    def test_simulator_run(self):
        self.assertRaises(NotImplementedError,self.simsimple.run)

class TestVodeSimulator(unittest.TestCase):
    """
    Testing the VodeSimulator
    """
    def test_vodesim_default(self):
        """
        test simple simulation with default settings

        the ODE for simplenet is:
        dx0/dt = 1 - k2*x0
        dx1/dt = k2*x0 - k3*x1
        the solution is:
        x0(t) = exp(-k2*t)*(x0(0)-1/k2) + 1/k2
        x1(t) = exp(-k3*t)*x1(0) + (k2*x0[0]-1)/(k3-k2)*(exp(-k2*t)-exp(-k3*t) + 1/k3*(1-exp(-k3*t))
            (if k2==k3 then the second summand is k2*exp(-k2*t)*(x0-1/k2)*t)
        """
        self.simplenet = generic.makesimplenet()
        x0 = [self.simplenet.values[i] for i in self.simplenet.species]
        p = [self.simplenet.values[i] for i in self.simplenet.parameters]
        self.simsimple = simulation.VodeSimulator(self.simplenet,range(31),x0,p)
        times,traj = self.simsimple.run()
        k2 = float(self.simplenet.values['k2'])
        k3 = float(self.simplenet.values['k3'])
        for t,x in zip(times,traj):
            # the exactness of scipy seems not to be reproducible among versions, so this
            # test is only within 4 places
            self.assertAlmostEqual(x[0],math.exp(-k2*t)*(self.simplenet.values['A']-1./k2) + 1./k2,places=4)
            x1 = ( math.exp(-k3*t)*self.simplenet.values['B'] +
                   ( (k2*self.simplenet.values['A']-1.)/(k3-k2)*(math.exp(-k2*t)-math.exp(-k3*t)) if k2!=k3 else
                     k2*math.exp(-k2*t)*(self.simplenet.values['A']-1./k2)*t ) +
                   1./k3*(1-math.exp(-k3*t)) )
            self.assertAlmostEqual(x[1],x1,places=4)

    def test_vodesim_conservation(self):
        """
        test whether VodeSimulator respects conservation relations
        """
        self.conservnet = generic.make_conservation_net()
        x0 = [self.conservnet.values[i] for i in self.conservnet.species]
        p = [self.conservnet.values[i] for i in self.conservnet.parameters]
        self.simconserv = simulation.VodeSimulator(self.conservnet,range(31),x0,p)
        times,traj = self.simconserv.run()
        for t,x in zip(times,traj):
            self.assertEqual(np.sum(x),np.sum(x0))
    
    def test_vodesim_custom_outputs(self):
        """
        test custom outputs in VodeSimulator
        """
        net = generic.makesimplenet()
        k2 = float(net.values['k2'])
        k3 = float(net.values['k3'])

        sim = simulation.VodeSimulator(net,range(31),outputs=("reactions",))
        times,traj = sim.run()
        for t,r in zip(times,traj):
            x0 = math.exp(-k2*t)*(net.values['A']-1./k2) + 1./k2
            x1 = ( math.exp(-k3*t)*net.values['B'] +
                   ( (k2*net.values['A']-1.)/(k3-k2)*(math.exp(-k2*t)-math.exp(-k3*t)) if k2!=k3 else
                     k2*math.exp(-k2*t)*(net.values['A']-1./k2)*t ) +
                   1./k3*(1-math.exp(-k3*t)) )
            v1 = 1.
            v2 = k2*x0
            v3 = k3*x1
            self.assertAlmostEqual(r[0],v1,places=4)
            self.assertAlmostEqual(r[1],v2,places=4)
            self.assertAlmostEqual(r[2],v3,places=4)

        sim = simulation.VodeSimulator(net,range(31),outputs=("reactions","variables"))
        times,trajr,trajv = sim.run()
        for t,r,v in zip(times,trajr,trajv):
            x0 = math.exp(-k2*t)*(net.values['A']-1./k2) + 1./k2
            x1 = ( math.exp(-k3*t)*net.values['B'] +
                   ( (k2*net.values['A']-1.)/(k3-k2)*(math.exp(-k2*t)-math.exp(-k3*t)) if k2!=k3 else
                     k2*math.exp(-k2*t)*(net.values['A']-1./k2)*t ) +
                   1./k3*(1-math.exp(-k3*t)) )
            v1 = 1.
            v2 = k2*x0
            v3 = k3*x1
            var = x1
            self.assertAlmostEqual(r[0],v1,places=4)
            self.assertAlmostEqual(r[1],v2,places=4)
            self.assertAlmostEqual(r[2],v3,places=4)
            self.assertAlmostEqual(v,var,places=4)

        sim = simulation.VodeSimulator(net,range(31),outputs="sin(A+B)")
        times,traj = sim.run()
        for t,v in zip(times,traj):
            x0 = math.exp(-k2*t)*(net.values['A']-1./k2) + 1./k2
            x1 = ( math.exp(-k3*t)*net.values['B'] +
                   ( (k2*net.values['A']-1.)/(k3-k2)*(math.exp(-k2*t)-math.exp(-k3*t)) if k2!=k3 else
                     k2*math.exp(-k2*t)*(net.values['A']-1./k2)*t ) +
                   1./k3*(1-math.exp(-k3*t)) )
            y = math.sin(x0+x1)
            self.assertAlmostEqual(v,y,places=4)

class TestVodeSimulatorStop(unittest.TestCase):
    def test_vodesimstop_symbolic(self):
        """
        test a symbolic stop condition
        """
        net = generic.makesimplenet()
        sim = simulation.VodeSimulatorStop(net, np.arange(10, step=0.1), stopcondition="A >= 0.5")
        t,x = sim.run()
        self.assertTrue(x[-1][0] >= 0.5)
        self.assertTrue(x[-2][0] < 0.5)

    def test_vodesimstop_lambda(self):
        """
        test a function stop condition
        """
        net = generic.makesimplenet()
        sim = simulation.VodeSimulatorStop(net, np.arange(10, step=0.1), stopcondition=lambda x:(x[0] >= 0.5))
        t,x = sim.run()
        self.assertTrue(x[-1][0] >= 0.5)
        self.assertTrue(x[-2][0] < 0.5)

    def test_vodesimstop_bool(self):
        """
        test a boolean stop condition
        """
        net = generic.makesimplenet()
        sim = simulation.VodeSimulatorStop(net, np.arange(10, step=0.1), stopcondition=False)
        t,x = sim.run()
        self.assertEqual(t[-1], 9.9)

class TestGillespieSimulator(unittest.TestCase):
    def test_gillespie_simplenet(self):
        """
        test simulation of simplenet with Gillespie
        """
        net = generic.makesimplenet()
        sim = simulation.GillespieSimulator(net, np.arange(10, step=0.1))
        t,x = sim.run()
        for xi in x:
            self.assertTrue(np.all(xi >= 0))
        for i in range(len(x)-1):
            self.assertTrue(np.all(x[i+1] == x[i]) or
                            mathutils.rank(net.stoich) == mathutils.rank(np.hstack((net.stoich,np.atleast_2d(x[i+1]-x[i]).T))))


class TestBrnSimulation(unittest.TestCase):
    def test_simplenet_sim(self):
        """
        test simple simulation with default settings

        the ODE for simplenet is:
        dx0/dt = 1 - k2*x0
        dx1/dt = k2*x0 - k3*x1
        the solution is:
        x0(t) = exp(-k2*t)*(x0(0)-1/k2) + 1/k2
        x1(t) = exp(-k3*t)*x1(0) + (k2*x0[0]-1)/(k3-k2)*(exp(-k2*t)-exp(-k3*t) + 1/k3*(1-exp(-k3*t))
            (if k2==k3 then the second summand is k2*exp(-k2*t)*(x0-1/k2)*t)
        """
        self.simplenet = generic.makesimplenet()
        x0 = [self.simplenet.values[i] for i in self.simplenet.species]
        p = [self.simplenet.values[i] for i in self.simplenet.parameters]
        times,traj = self.simplenet.simulate(range(31))
        k2 = float(self.simplenet.values['k2'])
        k3 = float(self.simplenet.values['k3'])
        for t,x in zip(times,traj):
            # the exactness of scipy seems not to be reducible among versions, so this
            # test is only within 4 places
            self.assertAlmostEqual(x[0],math.exp(-k2*t)*(self.simplenet.values['A']-1./k2) + 1./k2,places=4)
            x1 = ( math.exp(-k3*t)*self.simplenet.values['B'] +
                   ( (k2*self.simplenet.values['A']-1.)/(k3-k2)*(math.exp(-k2*t)-math.exp(-k3*t)) if k2!=k3 else
                     k2*math.exp(-k2*t)*(self.simplenet.values['A']-1./k2)*t ) +
                   1./k3*(1-math.exp(-k3*t)) )
            self.assertAlmostEqual(x[1],x1,places=4)

    def test_with_scale(self):
        """
        test simulation with scale factors xdot = S N v(x)
        """
        self.netlist = [brn.Brn(1,'x','v',rates={'v':'2-x'}),
                        brn.Brn(1,'x','v',rates={'v':'2-x'},scale=0.5),
                        brn.Brn(1,'x','v',rates={'v':'2-x'},parameters=['a'],parvals={'a':2},scale='a'),
                        brn.Brn(1,'x','v',rates={'v':'2-x'},variables=['a'],varvals={'a':'2'},scale='a'),
                        ]
        for n in self.netlist:
            n.set({'x':1})
        simres = [n.simulate(range(2)) for n in self.netlist]
        for res,nomres in zip(simres,[1.63212,1.39346,1.86466,1.86466]):
            self.assertEqual(res[0][-1],1.)
            self.assertAlmostEqual(res[1][-1][0],nomres,places=4)

    def test_mathexpr_simulation(self):
        net = generic.make_mathexpr_net()
        t,x = net.simulate(np.arange(20.))
        self.assertAlmostEqual(x[0],[math.sin(1)])
        self.assertAlmostEqual(x[-1],[0.70346743])

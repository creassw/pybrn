"""
test_mathutils - unittest suite for the module brn.mathutils
"""

import os
import brn.mathutils as bm
import unittest
import numpy as np

class TestRank(unittest.TestCase):
    """
    Testing the mathutils.rank function
    """
    def setUp(self):
        self.matrices = [np.eye(3)]
        self.matrices += [np.asarray([1,2,3])]
        self.matrices += [np.asarray([[1,2,3],[1,3,5]])]
        self.ranks = [3,1,2]
    
    def test_rank(self):
        for i,m in enumerate(self.matrices):
            self.assertEqual(bm.rank(m),self.ranks[i])

class TestNull(unittest.TestCase):
    """
    Testing the mathutils.null function
    """
    def setUp(self):
        A = np.eye(3)
        A[0,0] = 0
        B = np.array([[1, 0, 0],[0, 1, 0]])
        self.matrices = [np.eye(3), np.zeros((3,3)), A, B]
        self.nullspaces = [np.zeros((3,0)), np.eye(3), np.array([[1],[0],[0]]), np.array([[0],[0],[1]])]
    
    def test_rank(self):
        for i,m in enumerate(self.matrices):
            self.assertTrue(np.allclose(bm.null(m),self.nullspaces[i]))



"""
brn.test.test_cython_evaluator - unittest test suite for brn.brneval.CythonBrnEvaluator
"""
# Copyright (C) 2010 Steffen Waldherr waldherr@ist.uni-stuttgart.de
# Time-stamp: <Last change 2013-11-16 10:07:53 by Steffen Waldherr>

import os
import math
import brn
import brn.simulation
import unittest
import numpy as np

import generic

class TestCythonBrnEvaluator(unittest.TestCase):
    def test_cythonevaluator_simplenet(self):
        net = generic.makesimplenet()
        evl = brn.brneval.CythonBrnEvaluator(net)
        x0test = [[1,1],[2,2],[0,0],[1,2]]
        for x0 in x0test:
            z = x0 # no scaling nor conservation in simplenet
            rhs = evl.ode_rhs(z)
            for i,j in zip(rhs,np.dot(net.stoich,[1, x0[0], x0[1]])):
                self.assertEqual(i,j)
            jac = evl.ode_jac(z)
            self.assertTrue(np.all(jac == np.asarray([[-1,0],[1,-1]])))
        ptest = [[1,1],[1,2]]
        for p in ptest:
            evl.set(p=np.asarray(p,dtype=np.float64))
            rhs = evl.ode_rhs([1,1])
            for i,j in zip(rhs,np.dot(net.stoich,[1, p[0], p[1]])):
                self.assertEqual(i,j)

    def test_cythonevaluator_conservnet(self):
        net = generic.make_conservation_net()
        net.conservation_analysis()
        evl = brn.brneval.CythonBrnEvaluator(net)
        x0test = [[1,1,0],[2,2,0],[0,0,2],[1,2,3]]
        for x0 in x0test:
            evl.set(x0=x0)
            z = evl.x2z(x0) # only trafo, no scaling in conservnet
            rhs = evl.ode_rhs(z)
            rhs_x = np.dot(net.conserv.L,rhs)
            for i,j in zip(rhs_x,np.dot(net.stoich,[x0[0], x0[1]])):
                self.assertAlmostEqual(i,j)
            jac = evl.ode_jac(z)
            jac_z = np.dot(net.conserv.L,jac)
            jac_x = np.dot([[-1,0,0],[1,-1,0],[0,1,0]],net.conserv.L)
            self.assertTrue(np.all(jac_z - jac_x <= 1e-10))

class TestSundialsSimulator(unittest.TestCase):
    def test_sundials_simplenet(self):
        net = generic.makesimplenet()
        sim = brn.simulation.SundialsSimulator(net,np.arange(0.,7.,0.5))
        times,traj = sim.run()
        k2 = float(net.values['k2'])
        k3 = float(net.values['k3'])
        for t,x in zip(times,traj):
            # the exactness of scipy seems not to be reproducible among versions, so this
            # test is only within 4 places
            self.assertAlmostEqual(x[0],math.exp(-k2*t)*(net.values['A']-1./k2) + 1./k2,places=4)
            x1 = ( math.exp(-k3*t)*net.values['B'] +
                   ( (k2*net.values['A']-1.)/(k3-k2)*(math.exp(-k2*t)-math.exp(-k3*t)) if k2!=k3 else
                     k2*math.exp(-k2*t)*(net.values['A']-1./k2)*t ) +
                   1./k3*(1-math.exp(-k3*t)) )
            self.assertAlmostEqual(x[1],x1,places=4)

    def test_sundials_conservationnet(self):
        net = generic.make_conservation_net()
        x0 = [3,0,0]
        simconserv = brn.simulation.SundialsSimulator(net,np.arange(31.),x0)
        times,traj = simconserv.run()
        for t,x in zip(times,traj):
            self.assertAlmostEqual(np.sum(x),np.sum(x0),places=6)

    def test_sundials_mathexpr(self):
        net = generic.make_mathexpr_net()
        t,x = net.simulate(np.arange(20.),method=brn.simulation.SundialsSimulator)
        self.assertAlmostEqual(x[0],[math.sin(1)])
        self.assertAlmostEqual(x[-1],[0.70346743],places=6)
        
class TestSundialsSimulatorStop(unittest.TestCase):
    def test_sundialsstop_simplenet(self):
        net = generic.makesimplenet()
        sim = brn.simulation.SundialsSimulatorStop(net,np.arange(0.,2.5,0.2),stopcondition="C - 0.5")
        times,traj = sim.run()
        ind = net.species.index("B")
        self.assertAlmostEqual(traj[-1][ind], 0.5)
        self.assertTrue(times[-1] < 2.)

class TestSimulationCompile(unittest.TestCase):
    def runTest(self):
        self.test_simulation_compile_simplenet()
        
    def test_simulation_compile_simplenet(self):
        net = generic.makesimplenet()
        m = "simplenet"
        brn.simulation.compile_model(net, m, builddir="/tmp")
        sim = brn.simulation.SundialsSimulator(net, times=np.arange(10.), precompiled=os.path.join("/tmp",m))
        times,traj = sim.run()
        k2 = float(net.values['k2'])
        k3 = float(net.values['k3'])
        for t,x in zip(times,traj):
            # the exactness of scipy seems not to be reproducible among versions, so this
            # test is only within 4 places
            self.assertAlmostEqual(x[0],math.exp(-k2*t)*(net.values['A']-1./k2) + 1./k2,places=4)
            x1 = ( math.exp(-k3*t)*net.values['B'] +
                   ( (k2*net.values['A']-1.)/(k3-k2)*(math.exp(-k2*t)-math.exp(-k3*t)) if k2!=k3 else
                     k2*math.exp(-k2*t)*(net.values['A']-1./k2)*t ) +
                   1./k3*(1-math.exp(-k3*t)) )
            self.assertAlmostEqual(x[1],x1,places=4)

def makesuite():
    return unittest.defaultTestLoader.loadTestsFromModule(__import__("test_cython"))

if __name__ == "__main__":
    suite = makesuite()
    unittest.TextTestRunner(verbosity=2).run(suite)

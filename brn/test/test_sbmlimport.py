"""
brn.test.testbrn - unittest test suite for brn/brn.py

Copyright (C) 2010 Steffen Waldherr waldherr@ist.uni-stuttgart.de
"""

import os
import brn
import unittest
import numpy as np

class TestSbmlImport(unittest.TestCase):
    """
    Testing direct SBML import
    """
    def test_sbml_simplenet(self):
        """
        testing import of simplenet.xml
        """
        net = brn.fromSBML(os.path.join("test","data","simplenet.xml"))
        self.assertEqual(net.species,["A","B"])
        self.assertEqual(net.parameters,["k2","k3","default_compartment"])
        self.assertEqual(net.reactions,["v1","v2","v3"])
        self.assertEqual(net.values,{"A":0., "B":0., "k2":1., "k3":1., "default_compartment":1., "v1":"1", "v2":"k2 * A", "v3":"k3 * B"})
        self.assertFalse(net.has_conservation)
        self.assertTrue(net.has_scale)
        self.assertTrue(np.all(net.stoich==np.asarray([[1,-1,0],[0,1,-1]])))

    def test_sbml_functiondefinition1(self):
        """
        testing import of a network with function definition
        """
        net = brn.fromSBML(os.path.join("test","data","function-example-net.xml"))
        v = net.eval(net.reactions)
        self.assertEqual(v[0], 2.0)

    def test_sbml_functiondefinition2(self):
        """
        testing import of a network with function definition
        """
        net = brn.fromSBML(os.path.join("test","data","function-example-net2.xml"))
        v = net.eval(net.reactions)
        self.assertEqual(v[0], 2.0)

    def test_sbml_variablenet(self):
        """
        Testing import of a network with variable definitions.
        """
        net = brn.fromSBML(os.path.join("test","data","variables-example.xml"))
        v = net.eval(net.reactions)
        self.assertEqual(v[0], 0.1)
        self.assertEqual(v[1], 0.0)
        s2 = net.eval("S2", values={"Keq":4.0, "T":1.0})
        self.assertEqual(s2, 0.2)

class TestFunctionHelper(unittest.TestCase):
    """
    Testing SBML function definitions.
    """
    def test_functionhelper_sbml(self):
        """
        Testing function handling with expample file.
        """
        sbml = brn.sbmlimport.sbml
        reader = sbml.SBMLReader();
        doc = reader.readSBML(os.path.join("test","data","function-example-net.xml"));
        model = doc.getModel()
        fh = brn.sbmlimport.FunctionHelper(model)
        resstr = fh.process("f(2 * y)")
        self.assertEqual(resstr, "(2 * y) * 2")
        
        
                                     

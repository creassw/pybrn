"""
brn.test.test_brn_evaluator - unittest test suite for brn/brneval.py
"""
# Copyright (C) 2010 Steffen Waldherr waldherr@ist.uni-stuttgart.de
# Time-stamp: <Last change 2012-09-27 20:21:03 by Steffen Waldherr>

import os
import brn
import brn.brneval
import unittest
import numpy as np
import random

import generic

class TestExpressionEvaluator(unittest.TestCase):
    """
    test the ExpressionEvaluator class
    """
    def setUp(self):
        stoich = np.eye(2)
        species = ['s1','s2']
        reactions = ['r1','r2']
        parameters = ['p1','p2']
        variables = ['v1','v2']
        net = brn.Brn(stoich,species,reactions,parameters,variables)
        self.evl = brn.brneval.ExpressionEvaluator(net)

    def test_brn_eval_allnumerical(self):
        values = dict()
        for i in self.evl.net.values:
            values[i] = random.random()
        self.evl.set(values=values)
        for i,v in values.items():
            res = self.evl.evalexpression(i)
            self.assertEqual(res,v)

    def test_numerical_expressions(self):
        self.assertEqual(self.evl.evalexpression(0.0),0.0)
        self.assertEqual(self.evl.evalexpression([0.0])[0],0.0)
        self.evl.set(values={"p1":1.0})
        res = self.evl.evalexpression([0.0, "p1"])
        for r,e in zip(res, [0.0,1.0]):
            self.assertEqual(r,e)
    
    def test_brn_eval_varsymbolic(self):
        values = dict(zip(self.evl.net.species+self.evl.net.reactions
                          +self.evl.net.parameters+self.evl.net.variables,
                 [1,1]+[2,2]+[3,3]+['p1*s1','p2*r2']))
        self.evl.set(values=values)
        res = self.evl.evalexpression(self.evl.net.variables) 
        self.assertEqual(list(res),[3,6])

    def test_brn_eval_levels(self):
        net = generic.makesimplenet()
        evl = brn.brneval.ExpressionEvaluator(net, level="param")
        self.assertEqual(evl.evalexpression("k2+k3"), 2.0)
        self.assertRaises(Exception, lambda : evl.evalexpression("k2*A"))
        evl = brn.brneval.ExpressionEvaluator(net, level="species")
        self.assertEqual(evl.evalexpression("k2+k3*A"), 1.0)
        self.assertRaises(Exception, lambda : evl.evalexpression("k2+k3*C"))
        evl = brn.brneval.ExpressionEvaluator(net, values={"A":1}, level="all")
        self.assertEqual(evl.evalexpression("k2+k3*C"), 1.0)
        self.assertEqual(evl.evalexpression("k2+v1"), 2.0)

    def test_variables(self):
        net = generic.make_variables_net()
        evl = brn.brneval.ExpressionEvaluator(net)
        self.assertEqual(evl.evalexpression("v2"), 1.0)
        self.assertRaises(ValueError, lambda: evl.evalexpression("v3"))
        

class TestBrnEvaluator(unittest.TestCase):
    """
    test the BrnEvaluator class
    """
    def setUp(self):
        stoich = np.eye(2)
        species = ['s1','s2']
        reactions = ['r1','r2']
        parameters = ['p1','p2']
        variables = ['v1','v2']
        self.net = brn.Brn(stoich,species,reactions,parameters,variables)

    def test_evaluator_no_values(self):
        evl = brn.brneval.BrnEvaluator(self.net)
        self.assertEqual(list(evl.p),[0,0])
        self.assertEqual(list(evl.z0),[0,0])
        self.assertFalse(evl.have_conservation)
        self.assertFalse(evl.have_scale)
        self.assertRaises(NotImplementedError,evl.ode_rhs,None)
        self.assertRaises(NotImplementedError,evl.ode_jac,None)
        self.assertEqual(evl.convert_points([0,0]),[0,0])
        self.assertEqual(evl.convert_points([[0,0],[0,0]]),[[0,0],[0,0]])

    def test_evaluator_with_value_dict(self):
        net = generic.make_scaled_net()
        evl = brn.brneval.BrnEvaluator(net)
        evl.set(values={"k1":4})
        self.assertAlmostEqual(evl.p[0], 4.0)
        self.assertAlmostEqual(evl.common_scale*evl.scale[0], 0.25)
        evl.set(x0=[0, 0], values={"x2":2})
        self.assertAlmostEqual(evl.z0[1], 2.0*evl.scale[1])

        net = generic.make_conservation_net()
        evl = brn.brneval.BrnEvaluator(net)
        evl.set(x0=[1,1,1], values={"x2":2.0})
        x0expected = [1.0, 1.0, 2.0]
        x0actual = evl.convert_points(evl.z0)
        for i,j in zip(x0actual, x0expected):
            self.assertAlmostEqual(i,j)
            
    def test_brn_eval_modelcheck(self):
        net = generic.makesimplenet()
        net.values["k2"] = np.nan
        self.assertRaises(ValueError, brn.brneval.BrnEvaluator, net)

class TestPythonBrnEvaluator(unittest.TestCase):
    def test_pythonevaluator_simplenet(self):
        net = generic.makesimplenet()
        evl = brn.brneval.PythonBrnEvaluator(net)
        x0test = [[1,1],[2,2],[0,0],[1,2]]
        for x0 in x0test:
            z = x0 # no scaling nor conservation in simplenet
            rhs = evl.ode_rhs(z)
            for i,j in zip(rhs,np.dot(net.stoich,[1, x0[0], x0[1]])):
                self.assertEqual(i,j)
            jac = evl.ode_jac(z)
            self.assertTrue(np.all(jac == np.asarray([[-1,0],[1,-1]])))

    def test_pythonevaluator_conservnet(self):
        net = generic.make_conservation_net()
        net.conservation_analysis()
        evl = brn.brneval.PythonBrnEvaluator(net)
        x0test = [[1,1,0],[2,2,0],[0,0,2],[1,2,3]]
        for x0 in x0test:
            evl.set(x0=x0)
            z = evl.x2z(x0) # only trafo, no scaling in conservnet
            rhs = evl.ode_rhs(z)
            rhs_x = np.dot(net.conserv.L,rhs)
            for i,j in zip(rhs_x,np.dot(net.stoich,[x0[0], x0[1]])):
                self.assertAlmostEqual(i,j)
            jac = evl.ode_jac(z)
            jac_z = np.dot(net.conserv.L,jac)
            jac_x = np.dot([[-1,0,0],[1,-1,0],[0,1,0]],net.conserv.L)
            self.assertTrue(np.all(jac_z - jac_x <= 1e-10))

    def test_integer_division(self):
        net = brn.Brn([1])
        net.set({"v0":"1 / 2 * x0"})
        v0 = net.eval("v0", values={"x0":1})
        self.assertEqual(v0, 0.5)
    
        
def makesuite():
    return unittest.defaultTestLoader.loadTestsFromModule(__import__("test_brn_evaluator"))

if __name__ == "__main__":
    suite = makesuite()
    unittest.TextTestRunner(verbosity=2).run(suite)

"""
provides the class Brn, representing a biochemical reaction network.
"""
# Copyright (C) 2010 Steffen Waldherr waldherr@ist.uni-stuttgart.de
# Time-stamp: <Last change 2013-05-15 17:31:14 by Steffen Waldherr>

# HINT:
# string array evaluation: np.vectorize(lambda x:eval(x,{},{'k':2}))([['k']])

from __future__ import division

import numpy as np
import re
import math

import warnings

import brncomp
import brneval
import conserv
import mathutils
import steadystate
import simulation

class Brn(object):
    """
    This class represents a biochemical reaction network, specifically the ODE of the form
    xdot = N * v(x), where N is the stoichiometric matrix, v the reaction rate vector, and
    x the species concentration vector.

    methods:
    Brn.addelements - add reactions, species, and/or parameters to the BRN
    Brn.conservation_analysis - analyse conservation relations
    Brn.eval - evaluate a symbolic expressions in the context of this BRN
    Brn.jacobian - compute a Jacobian matrix
    Brn.rate_fun - generate a lambda function for the rate vector
    Brn.scale_fun - generate a lambda function for the scale matrix
    Brn.set - set values for this BRN
    Brn.showreact - display a reaction as string
    Brn.simulate - simulate the network's ODE
    Brn.steadystate - compute a steady state
    Brn.steadystate_branch - compute a branch of steady states by varying
         one parameter

    properties:
    Brn.has_conservation - flag indicating whether conserved moieties are present
    Brn.has_scale - flag indicating whether a non-identiy scale factor is present
    """

    def __init__(self,stoich,species=None,reactions=None,parameters=[],variables=[],rates={},ics={},
                 parvals={},varvals={},scale=1.):
        """
        BRN constructor.
        
        Required arguments:
        - stoich: stoichiometric matrix of the network

        Optional arguments:
        - species: list of species names
        - reactions: list of reaction names
        - parameters: list of parameter names
        - variables: list of variables names
        - rates: dict of reaction rate expressions
          keys are reaction names, values are strings of numbers
        - ics: dict of initial conditions
          keys are species names, values are numbers for initial concentrations / amount
        - parvals: dict of parameter values
          keys are parameter names, values are numbers or symbolic expressions depending on
          numerical parameters
        - varvals: dict of variables values
          keys are variables names, values are numbers or strings (symbolic expressions)
        - scale: used to scale units of reaction rates to species change
          can be a string, scalar, or list of strings/scalars
          strings are interpreted as symbolic expressions
          this argument changes the ODE of the network:
            xdot = scale * stoich * reactionrates

        The values for parameters, species initial conditions, variables and reaction rates
        can be given as numbers or strings representing symbolic expressions.
        If symbolic expressions are used, the following evaluation order needs to be taken
        into account:
        1. Formulas for parameters may only depend on parameters with numerical values
        2. Formulas for species initial conditions may depend on parameters or other
           species with numerical values
        3. Variables may depend on parameters, species, and other variables with numerical
           values
        4. Reaction rate formulas may depend on parameters, species, variables, and other
           rates with numerical values
        """
        self.stoich = np.copy(np.atleast_2d(stoich))

        if species is None:
            self.species = ['x'+str(i) for i in xrange(self.stoich.shape[0])]
        else:
            self.species = [i for i in species]
        if reactions is None:
            self.reactions = ['v'+str(i) for i in xrange(self.stoich.shape[1])]
        else:
            self.reactions = [i for i in reactions]
        self.parameters = [i for i in parameters]
        self.variables = [i for i in variables]

        self.values = dict.fromkeys(self.species,0)
        [self.values.setdefault(i,0) for i in self.reactions]
        [self.values.setdefault(i,0) for i in self.parameters]
        [self.values.setdefault(i,0) for i in self.variables]
        self.set(rates)
        self.set(ics)
        self.set(parvals)
        self.set(varvals)
        if np.isscalar(scale):
            self.scale = [scale for i in xrange(len(self.species))]
        else:
            self.scale = [s for s in scale]

    def __str__(self):
        """
        generate a string representation of the network
        """
        mystr = "Reactions:\n"
        mystr += "\n".join([self.showreact(v,printstr=False) for v in self.reactions])
        mystr += "\n\nSpecies initial conditions:\n"
        mystr += "\n".join([x + " = " + str(self.values[x]) for x in self.species])
        if len(self.parameters)>0:
            mystr += "\n\nParameter values:\n"
            mystr += "\n".join([x + " = " + str(self.values[x]) for x in self.parameters])
        if len(self.variables)>0:
            mystr += "\n\nVariables values:\n"
            mystr += "\n".join([x + " = " + str(self.values[x]) for x in self.variables])
        return mystr
    
    def set(self,values,strict=True):
        """
        Assign values to symbols in the network from the dict values

        Usage example:
        >>> net.set({'k1':1}) # sets parameter k1 to 1

        Set strict to False to avoid exceptions when key in values are
        not known in the network.
        """
        if strict:
            netvals = self.parameters+self.species+self.variables+self.reactions
            for k in values.keys():
                if k not in netvals:
                    raise ValueError("%s is not a known variable in this BRN." % k)
        self.values.update(values)

    def addelements(self, reactions=[], stoich={}, species=[], parameters=[], values={}, scale=1.):
        """
        add the given reactions, species, and parameters (sequences of strings) to the network
        reaction rates and other values may be given in the dict ''values''

        If reactions is non-empty, then ''stoich'' is a dictionary of dictionaries
        describing the stoichiometry of the reactions. For example, if reaction ''v'' is
        added, and the network contains species A which is converted to B through v, then
        stoich = {'v':{'A':-1, 'B':1}}
        """
        if parameters.__class__ is "".__class__:
            parameters = [parameters]
        if species.__class__ is "".__class__:
            species = [species]
        if reactions.__class__ is "".__class__:
            reactions = [reactions]
        self.parameters += parameters
        if len(species) > 0:
            self.species += species
            self.stoich = np.vstack((self.stoich,np.zeros((len(species),len(self.reactions)))))
            if np.isscalar(scale):
                self.scale += [scale for i in species]
            else:
                self.scale += [s for s in scale]
        if len(reactions) > 0:
            self.reactions += reactions
            stoichmat = np.atleast_2d([[stoich[v].get(s,0.) if v in stoich else 0.
                                        for v in reactions]
                                       for s in self.species])
            self.stoich = np.hstack((self.stoich, stoichmat))
        self.set(values)
        if "conserv" in self.__dict__:
            self.conservation_analysis()
    
    def eval(self,expr,valuedict={},p=None,x=None,values={},level="all",**kwargs):
        """
        evaluate a symbolic expression, given as string 'expr', for this BRN
        state and parameter values may be given in the dictionary 'values', or
          in the vectors 'x' for state variables and 'p' for parameters.
          the values in 'values' take precedence over the vectors.
          for unspecified variables default values are used.

        see the documentation of brneval.ExpressionEvaluator for more info.
        """
        if len(valuedict):
            warnings.warn("Argument 'valuedict' is deprecated, use 'values' instead.",DeprecationWarning)
            if not len(values):
                values = valuedict
        return brneval.ExpressionEvaluator(self,p,x,values,level=level, **kwargs).evalexpression(expr)
    
    def evalparam(self,expr,p=None,values={}):
        """
        evaluate a symbolic expression, given as string 'expr', which only
          depends on the network's parameters, for this BRN
        parameter values may be given in the dictionary 'values' or in the vector 'p'.
          for unspecified variables default values are used.
        """
        return self.eval(expr, p=p, values=values, level="param")

    def showreact(self,react,printstr=True):
        """
        generate nice string representations of the reaction 'react'
        if 'printstr' is True, output the generated strings via the print method

        usage:
        >>> strings = net.showreact('v1')
        """
        mu = self.stoich[:,self.reactions.index(react)]
        substratestr = "+".join(("%g*%s" % (-m,self.species[i]) for i,m in enumerate(mu) if m<0))
        productstr = "+".join(("%g*%s" % (m,self.species[i]) for i,m in enumerate(mu) if m>0))
        reactstr = str(react) + ": " + substratestr + " -> " + productstr + "; rate: " + str(self.values[react])
        if printstr:
            print reactstr
        return reactstr

    def rate_fun(self, values={}, returntype='fun'):
        """
        generate a lambda function for the reaction rates v(x,p)
        argument to the generated function are lists of species and parameter values

        Custom mathematical expressions for reaction rate or variables can be
        passed in 'values'.

        Options for 'returntype':
        fun - return a callable function
        str - return the string of the function definition

        usage:
        >>> fun = net.rate_fun()
        >>> fun([1],[])
        -1.0
        """
        defsstr = ""
        for i,s in enumerate(self.species):
            defsstr += '%s = species_vector[%d]\n    ' % (s,i)
        for i,p in enumerate(self.parameters):
            defsstr += '%s = param_vector[%d]\n    ' % (p,i)
        for v in self.variables:
            vstr = str(values[v]) if v in values else str(self.values.get(v,0))
            for i in math.__dict__:
                if i not in self.values:
                    vstr = re.sub(r'(\W|^)(?<!math\.)'+i+r'(\W|$)',r'\1math.'+i+r'\2',vstr)
            defsstr += '%s = %s\n    ' % (v,vstr)
        ratestr = "["
        for i,r in enumerate(self.reactions):
            rstr = str(values[r]) if r in values else str(self.values.get(r,0))
            for i in math.__dict__:
                if i not in self.values:
                    rstr = re.sub(r'(\W|^)(?<!math\.)'+i+r'(\W|$)',r'\1math.'+i+r'\2',rstr)
            ratestr += '%s, ' % rstr
        ratestr += ']'
        rfunstr = (('def ratefun(species_vector,param_vector):\n' +
                    '    %s\n' +
                    '    rate_vector = np.asarray(%s,dtype=np.float64)\n' +
                    '    return rate_vector') %
                   (defsstr, ratestr) )
        if returntype == "str":
            return rfunstr
        exec(rfunstr)
        return ratefun

    @property
    def has_scale(self):
        """
        returns a Boolean flag which is true if the network has defined a
        non-identity scale factor (xdot = scale * N * v(x)), and false
        otherwise.
        """
        return not np.alltrue([s==1 for s in self.scale])

    def scale_fun(self):
        """
        generate a lambda function for the scaling factor
        argument to the generated function is a list of parameter values

        usage:
        >>> fun = net.scale_fun()
        >>> fun([])
        1
        """
        # substitute variables in scale
        scale = [s for s in self.scale]
        for i in self.variables:
            for j,s in enumerate(scale):
                if s.__class__ is "".__class__:
                    scale[j] = re.sub(r'(\W|^)'+i+r'(\W|$)',r'\1('+str(self.values[i])+r')\2',s)
        scalestr = re.sub(r'\'','',str(scale))
        # substitute arguments in scale
        plist = ["plistarg["+str(i)+"]" for i in xrange(len(self.parameters))]
        for a,b in zip(plist,self.parameters):
            scalestr = re.sub(r'(\W|^)'+b+r'(\W|$)',r'\1'+a+r'\2',scalestr)
        lambdastr = "lambda plistarg: " + scalestr
        return eval(lambdastr,{},{})        
    
    def jacobian(self,fun,var,returntype='fun'):
        """
        compute a jacobian of the brn, either as lambda function or as symbolic expression

        arguments:
        fun - symbolic expression which the Jacobian should be taken of
        var - variables in which the differentiation is done
        returntype - specify one of 'fun' or 'str'

        Usage examples:
        >>> jac = net.jacobian(net.reactions,net.species)
        >>> jac(net.eval(net.species),net.eval(net.parameters))
        """
        jac = brncomp.jacobian(fun,var,[self.species,self.parameters],self.values,returntype=returntype)
        return jac

    def linearapproximation(self, x=None, p=None, values={}, inputs=None, outputs=None):
        """
        compute matrices for a linear approximation this Brn.

        Arguments:
        values - dict to specify custom species and parameter values
        x, p - custom species, parameter values, takes precedence over values
        inputs - list of parameters to be considered as input variables
        outputs - list of symbolic expressions to be considered as output variables

        Compute the linear approximations A = df/dz, B = df/du, and C = dh/dx, where f is
        the network's ODE right hand side, z the state variable, u the input variables,
        and h the output functions, evaluated at the specified species concentrations
        and parameter values

        Returns (A,B,C), where B and / or C are None if inputs / outputs is.
        """
        if x is None or p is None:
            evl = brneval.ExpressionEvaluator(self, values=values, level="species")
        myx = evl.evalexpression(self.species) if x is None else x
        myp = evl.evalexpression(self.parameters) if p is None else p
        return brncomp.linearapproximation(self, myx, myp, inputs, outputs, returntype="num")

    def conservation_analysis(self, store=True):
        """
        do a conservation analysis of the network.
        
        generates the attribute 'conserv' in this Brn object which is a
        brn.conserv.Conservation object, containing all information about
        conservation relations.
        The generated Conservation object is also returned by the method.

        See the documentation of brn.conserv.Conservation for further information.
        """
        cons = conserv.Conservation(self)
        if store:
            self.conserv = cons
        return cons

    @property
    def has_conservation(self):
        """
        returns true if the network has conserved moieties, false otherwise.
        """
        return mathutils.rank(self.stoich) < self.stoich.shape[0];

    def steadystate(self,p=None,x0=None,values={}):
        """
        try to compute a steady state of the Brn

        output:
        xs - result of steady state computation

        custom initial conditions for species and parameter values can be given in 'x0' or 'p', respectively
        state and parameter values may also be updated via the dict 'values'
        information from 'x0' and 'p' takes precedence over 'values'
        """
        x,info = steadystate.SteadyState(self,p,x0,values).compute()
        return x

    def steadystate_branch(self,par,start,stop,step=0,p=None,x0=None,values={}):
        """
        simplistic computation of a steady state branch:
        varies the value of parameter 'par' from 'start' to 'stop', increasing the
        value in steps of length 'step'
        if 'step' is 0, make a total of 100 steps from start to stop

        custom initial conditions for species and parameter values can be given in 'x0' or 'p', respectively
        state and parameter values may also be updated via the dict 'values'
        information from 'x0' and 'p' takes precedence over 'values'        

        output:
        plist - list of parameter values for which steady states have been computed
        xslist - list of steady states, corresponding to parameter values in plist
        """
        if x0 is None or p is None:
            evl = brneval.ExpressionEvaluator(self, values=values, level="species")
        myx0 = evl.evalexpression(self.species) if x0 is None else x0
        myp = evl.evalexpression(self.parameters) if p is None else p
        if not step:
            step = float(stop-start)/100
        plist, xslist, info = steadystate.steadystate_branch(self,par,start,stop,step,myp,myx0)
        return plist, xslist

    def simulate(self,t,p=None,x0=None,values={},method=simulation.VodeSimulator,**args):
        """
        simulate the network

        arguments:
        t - list of time points for which simulation data should be generated
        method - which implementation of brn.simulation.Simulator to use
        args - keyword arguments which are passed to the simulator upon initialization
               see e.g. help(brn.simulation.Simulator) and subclasses
        """
        times = t
        if x0 is None or p is None:
            evl = brneval.ExpressionEvaluator(self, values=values, level="species")
        myx0 = evl.evalexpression(self.species) if x0 is None else x0
        myp = evl.evalexpression(self.parameters) if p is None else p
        sim = method(self,times,myx0,myp,**args)
        tres, xres = sim.run()
        return tres, xres


def make_massaction_network(stoich, species=None, reactions=None, parameters=None, paramvalues=None, ics=None):
    """
    generate a Brn with standard mass action reaction rates from the stoichiometric matrix 'stoich'.

    This function builts the network with irreversible reactions only!

    Optional arguments can be given as list of species names, reaction names, parameter names,
    numerical parameter values, and numerical initial conditions for species concentrations.
    If species, reaction, or parameter names are not given, they default to
    x1, x2, ...,
    v1, v2, ..., and
    k1, k2, ..., respectively.
    If parameter values or initial conditions are not given, they default to None.

    Note that parameter values and initial conditions can be changed later via the Brn.set() method.
    """
    stoich = np.atleast_2d(stoich)
    if species is None:
        species = ["x%d" % (i+1) for i in xrange(stoich.shape[0])]
    elif len(species) != stoich.shape[0]:
        raise ValueError("'species' optional argument must be a list of length %d." % stoich.shape[0])
    if reactions is None:
        reactions = ["v%d" % (i+1) for i in xrange(stoich.shape[1])]
    elif len(reactions) != stoich.shape[1]:
        raise ValueError("'reactions' optional argument must be a list of length %d." % stoich.shape[1])
    if parameters is None:
        parameters = ["k%d" % (i+1) for i in xrange(stoich.shape[1])]
    elif len(parameters) != stoich.shape[1]:
        raise ValueError("'parameters' optional argument must be a list of length %d." % stoich.shape[1])
    rates = {}
    for j,r in enumerate(reactions):
        rates[r] = "*".join([parameters[j]] +
                            [("%s**%g" % (x, -n) if n != -1 else x) for x,n in zip(species, stoich[:,j]) if n < 0])
    icsdict = dict.fromkeys(species, 0.0) if ics is None else dict(zip(species, ics))
    parvaldict = dict.fromkeys(parameters, 0.0) if paramvalues is None else dict(zip(parameters, paramvalues))
    return Brn(stoich, species, reactions, parameters, rates=rates, ics=icsdict, parvals=parvaldict)





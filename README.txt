=====
pybrn
=====

pybrn is a Python package for the analysis of biochemical reaction
networks. It is mainly meant as a basic library for researchers
developing their own model analysis routines in Python. pybrn
currently features:
- basic model creation, data handling and evaluation
- import of SBML files into pybrn's data structures
- analysis of network conservation relations
- computation of steady states and steady state branches
- integration of the network's differential equation

The following interactive Python session shows some of the basic
features::

  >>> import brn
  >>> net = brn.fromSBML("doc/examples/simplenet.xml")
  >>> print net
  Reactions:
  v1:  -> 1*A; rate: 1
  v2: 1*A -> 1*B; rate: k2 * A
  v3: 1*B -> ; rate: k3 * B
  <BLANKLINE>
  Species initial conditions:
  A = 0.0
  B = 0.0
  <BLANKLINE>
  Parameter values:
  k2 = 1.0
  k3 = 1.0
  default_compartment = 1.0
  >>> net.steadystate()
  array([ 1.,  1.])
  >>> t,x = net.simulate(list(xrange(11)))
  Found integrator vode
  >>> print t[-1], x[-1]
  10.0 [ 0.9999546  0.9995006]


Installation
============

Software requirements
---------------------

Required software:
- Python version 2.x with x >= 5.
- NumPy: http://www.numpy.org/

Recommended software:
- libSBML with Python bindings version 3.4.1 or later:
  http://sbml.org/Software/libSBML
  required for importing SBML models
- SciPy: http://www.scipy.org
  required for simulation and steady state computation
- sympy: http://code.google.com/p/sympy/
  required for Jacobian computation and for using the Jacobian in
  numerical computations

Optional software:
- Cython 0.14 or later:
  http://www.cython.org
  can be used for efficient network evaluation
- SUNDIALS library 2.3.0 with headers:
  https://computation.llnl.gov/casc/sundials/main.html
  can be used in combination with Cython for efficient network
  simulation

Installation instructions
-------------------------

pybrn is installed via the Python setuptools. For standard
installation, simply run the following command in the package's root
directory::

  python setup.py install

Depending on your system configuration, you may need administrator
rights to do the installation.

Testing the installation
------------------------

In order to test whether everything works, e.g. all required and
recommended packages are available, you can run the following test
command before installing the package::

  python test/test_all.py

To test the minimal functionality without the need for any of the
recommended or optional packages, use the command::

  python test/test_minimal.py

Usage
=====

The basic usage of pybrn is described in the ``TUTORIAL.txt`` file in
the ``docs`` subdirectory. Most of the functions are also documented
within the Python online help system, which can for example be
accessed by the command ``help(brn)``.


Copyright
=========

pybrn is (C) 2010-2013 by Steffen Waldherr, steffen.waldherr@ovgu.de

The package is licensed under the GPL v3, see the file LICENSE.txt for
more information.

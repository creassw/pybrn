#!/usr/bin/env python
"""
Distutils based setup script for pybrn.

This uses Distutils (http://python.org/sigs/distutils-sig/) the standard
python mechanism for installing packages. For the easiest installation
just type the command (you'll probably need root privileges for that):

    python setup.py install

This will install the library in the default location. For instructions on
how to customize the install procedure read the output of:

    python setup.py --help install

To get a full list of avaiable commands, read the output of:

    python setup.py --help-commands
"""

from distutils.core import setup;

setup(name = 'pybrn',
      version = '0.4.3',
      description='Analysis of biochemical reaction networks',
      author='Steffen Waldherr',
      author_email='steffen.waldherr@ovgu.de',
      url='http://sourceforge.net/projects/pybrn/',
      packages = ['brn','brn.test']
      )

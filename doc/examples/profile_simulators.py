#!/usr/bin/env python
"""
profile_simulators.py: compare the different simulator implementations
"""

import numpy as np
import matplotlib.pyplot as mpl
import logging
import cProfile, pstats

import brn
import brn.simulation
import brn.brneval

logging.basicConfig(level=logging.INFO,
                    format='%(asctime)s %(levelname)s %(message)s')

def main():
    pass

runs = 100
plotruns = 10

species = ['S','E1','C1','P1','E2','C2','P2']
reactions = ['v1','v2','v3','v4']
stoich = np.asarray([[-1,0,0,0],
                     [-1,1,0,0],
                     [1,-1,0,0],
                     [0,1,-1,0],
                     [0,0,-1,1],
                     [0,0,1,-1],
                     [0,0,0,1]
                     ])
parameters = ['k1p','k1m','k2','k3p','k3m','k4']
pvals = dict(zip(parameters,[1.,1.,1.,1.,1.,1.]))
ics = dict(zip(species,[10,1,0,0,1,0,0]))
rates = dict(zip(reactions,['k1p*S*E1 - k1m*C1','k2*C1','k3p*P1*E2 - k3m*C2','k4*C2']))
profnet = brn.Brn(stoich,species,reactions,parameters=parameters,rates=rates,ics=ics,parvals=pvals)

times = np.arange(0.,50.,0.01)

def test_vode_python(net,times):
    traj = []
    s = brn.simulation.VodeSimulator(net,times)
    for i in xrange(runs):
        logging.info("Vode-Simulation %d of %d..." % (i,runs))
        p = 10**(2*(np.random.random(len(net.parameters))-0.5))
        s.initialize(p=p)
        t,x = s.run()
        logging.info("finished.")
        traj.append((t,np.asarray(x)))
    fig = mpl.figure()
    ax = fig.add_subplot(111)
    indices = map(net.species.index,['S','P1','P2'])
    for i in xrange(plotruns):
        ax.plot(traj[i][0],traj[i][1][:,indices[0]],'r')
        ax.plot(traj[i][0],traj[i][1][:,indices[1]],'g')
        ax.plot(traj[i][0],traj[i][1][:,indices[2]],'b')

def test_vode_cython(net,times):
    traj = []
    s = brn.simulation.VodeSimulator(net,times,evaluator=brn.brneval.CythonBrnEvaluator)
    for i in xrange(runs):
        logging.info("CythonBrnEvaluator-Simulation %d of %d..." % (i,runs))
        p = 10**(2*(np.random.random(len(net.parameters))-0.5))
        s.initialize(p=p)
        t,x = s.run()
        logging.info("finished.")
        traj.append((t,np.asarray(x)))
    fig = mpl.figure()
    ax = fig.add_subplot(111)
    indices = map(net.species.index,['S','P1','P2'])
    for i in xrange(plotruns):
        ax.plot(traj[i][0],traj[i][1][:,indices[0]],'r')
        ax.plot(traj[i][0],traj[i][1][:,indices[1]],'g')
        ax.plot(traj[i][0],traj[i][1][:,indices[2]],'b')

def test_sundials(net,times):
    traj = []
    s = brn.simulation.SundialsSimulator(net,times)
    for i in xrange(runs):
        logging.info("CythonBrnEvaluator-Simulation %d of %d..." % (i,runs))
        p = 10**(2*(np.random.random(len(net.parameters))-0.5))
        s.initialize(p=p)
        t,x = s.run()
        logging.info("finished.")
        traj.append((t,np.asarray(x)))
    fig = mpl.figure()
    ax = fig.add_subplot(111)
    indices = map(net.species.index,['S','P1','P2'])
    for i in xrange(plotruns):
        ax.plot(traj[i][0],traj[i][1][:,indices[0]],'r')
        ax.plot(traj[i][0],traj[i][1][:,indices[1]],'g')
        ax.plot(traj[i][0],traj[i][1][:,indices[2]],'b')

cProfile.run('test_vode_python(profnet,times)','vode_python')
cProfile.run('test_vode_cython(profnet,times)','vode_cython')
cProfile.run('test_sundials(profnet,times)','sundials')

st = pstats.Stats('vode_cython')
st.sort_stats('cumulative').print_stats(10)
    
st = pstats.Stats('vode_python')
st.sort_stats('cumulative').print_stats(10)
    
st = pstats.Stats('sundials')
st.sort_stats('cumulative').print_stats(10)
    
mpl.show()



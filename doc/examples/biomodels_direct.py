"""
biomodels_direct.py: script to show direct interaction with the biomodels.net database

The script downloads the Huang-Ferrell MAPK model from the biomodels.net database,
does a simulation and displays the time courses for the various forms of the MAPK protein.
"""

import brn
import httplib
import numpy as np
import matplotlib.pyplot as mpl

h = httplib.HTTPConnection("www.ebi.ac.uk")
h.request("GET", "/biomodels-main/download?mid=BIOMD0000000009")
r = h.getresponse()
data = r.read()

mapk = brn.fromSBMLString(data)
t,x = mapk.simulate(np.arange(1001.)/10)

mpl.plot(t,np.asarray(x)[:,mapk.species.index('PP_K')])
mpl.plot(t,np.asarray(x)[:,mapk.species.index('P_K')])
mpl.plot(t,np.asarray(x)[:,mapk.species.index('K')])

mpl.show()


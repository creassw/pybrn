"""
splitreactions.py: module to split all sums in reactions and make them individual reactions

Provides the method split which returns the modified network.
Requires the sympy package.
"""

import brn
import numpy as np
import sympy as sym
import warnings

def split(net):
    """
    Return a new brn where reactions with sums are split into individual reactions.
    """
    varnames = net.parameters + net.species + net.variables
    vardict = dict((v, sym.Symbol(v)) for v in varnames)
    stoichnewT = []
    reactionsnew = []
    ratedict = {}
    for i,v in enumerate(net.reactions):
        rate = net.values[v]
        try:
            ratesym = sym.sympify(rate, locals=vardict).expand()
        except:
            warnings.warn("Sympify failed on rate: %s" % rate)
            reactionsnew.append(v)
            stoichnewT.append(net.stoich[:,i])
            ratedict[v] = rate
        else:
            if isinstance(ratesym, sym.add.Add):
                for j,a in enumerate(ratesym.args):
                    reactionsnew.append(v + "_" + str(j))
                    isbackreaction = isinstance(a, sym.mul.Mul) and isinstance(a.args[0], sym.numbers.NegativeOne)
                    stoichnewT.append(-net.stoich[:,i] if isbackreaction else net.stoich[:,i])
                    ratedict[reactionsnew[-1]] = str(-a if isbackreaction else a)
            else:
                reactionsnew.append(v)
                stoichnewT.append(net.stoich[:,i])
                ratedict[v] = rate
    newnet = brn.Brn(np.asarray(stoichnewT).T, net.species, reactionsnew, net.parameters, net.variables, scale=net.scale)
    newnet.set(dict((k,v) for k,v in net.values.items() if k in net.species))
    newnet.set(dict((k,v) for k,v in net.values.items() if k in net.parameters))
    newnet.set(dict((k,v) for k,v in net.values.items() if k in net.variables))
    newnet.set(ratedict)
    return newnet

"""
network.py: examples of brn structural analysis with networkx

Requires the networkx package.
"""
import warnings
from itertools import product

import networkx as nx
import numpy as np

import brn

class SRGraph(object):
    def __init__(self, net):
        """
        Initialize the species-reaction network graph from the brn net.
        """
        self.graph = nx.DiGraph()
        self.net = net
        self.graph.add_nodes_from(net.species, type="species")
        self.graph.add_nodes_from(net.reactions, type="reaction")
        for (s,r) in product(*map(range,net.stoich.shape)):
            if net.stoich[s,r]:
                self.graph.add_edge(net.reactions[r], net.species[s], weight=net.stoich[s,r])
        V = net.jacobian(net.reactions, net.species, returntype="str")
        for (r,s) in product(*map(range,net.stoich.T.shape)):
            if V[r][s] != '0':
                self.graph.add_edge(net.species[s], net.reactions[r], derivative=V[r][s])

    def draw(self):
        nx.draw(self.graph)

    def nontrivial_cycles(self):
        """
        Return list of non-trivial cycles in the SR-graph.
        Non-trivial cycles are cycles of length larger than 3, i.e., all cycles which do not simply stem
        from the change of a species concentration due to a reaction where the species is a substrate.
        """
        cycles = nx.simple_cycles(self.graph)
        return [c for c in cycles if len(c) > 3]

    def cycles_with_edge(self, edge):
        """
        Return list of non-trivial cycles which contain edge.
        edge is a tuple of nodes.
        """
        cycles = nx.simple_cycles(self.graph)
        cyclese = []
        for c in cycles:
            if len(c) > 3:
                try:
                    ind = c.index(edge[0])
                except:
                    continue
                if c[(ind + 1) % len(c)] == edge[1]:
                    cyclese.append(c)
        return cyclese

    def cycle_edgeweight(self):
        """
        For each species -> reaction interaction in the BRN, compute the number
        of cycles that this interaction takes part in.

        Returns:
        - A matrix of the same shape as the stoichiometric matrix, where the
        (s,r) entry corresponds to the number of cycles going through the interaction
        from s to r.
        - The total number of cycles.
        """
        cyccount = np.zeros(self.net.stoich.shape)
        cycles = nx.simple_cycles(self.graph)
        print "Found %d simple cycles." % len(cycles)
        for c in cycles:
            start = 0 if c[0] in self.net.species else 1
            for i in range(start,len(c)-1,2):
                cyccount[self.net.species.index(c[i]), self.net.reactions.index(c[i+1])] += 1
        return cyccount, len(cycles)


def test_simplenet():
    from brn.test import generic
    net = generic.makesimplenet()
    g = SRGraph(net)
    g.draw()
    return g
        

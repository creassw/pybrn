"""
test_interactive_examples: unittest suites for the interactive examples
"""
# Copyright (C) 2010 Steffen Waldherr waldherr@ist.uni-stuttgart.de
# Time-stamp: <Last change 2011-02-05 19:40:22 by Steffen Waldherr>

import doctest
import os.path

def test_readme():
    doctest.testfile(os.path.join("..","README.txt"))

def test_tutorial():
    doctest.testfile(os.path.join("..","doc","TUTORIAL.txt"))
